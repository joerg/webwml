#use wml::debian::template title="Поддержка"
#use wml::debian::toc
#use wml::debian::translation-check translation="01382b6032212911085d4338b57e4b0f7d98d4ae" maintainer="Lev Lamberov"

<define-tag toc-title-formatting endtag="required" whitespace="delete">
<h2>%body</h2>
</define-tag>

Debian и его поддержка предоставляется сообществом добровольцев.

Если поддержки сообщества недостаточно для ваших нужд, то вы можете
обратиться к нашей <a href="doc/">документации</a> или нанять
<a href="consultants/">консультанта</a>.

<toc-display/>

<toc-add-entry name="irc">Помощь в реальном времени через IRC</toc-add-entry>

<p><a href="http://www.irchelp.org/">IRC (Internet Relay Chat)</a>
&mdash; это способ общения людей со всего мира в
реальном времени. Информацию об IRC-каналах, так или иначе связанных с Debian,
вы можете найти на сервере
<a href="https://www.oftc.net/">OFTC</a>.</p>

<p>Для соединения с сервером Вам необходим IRC-клиент.
<a href="https://packages.debian.org/stable/net/hexchat">HexChat</a>,

<a href="https://packages.debian.org/stable/net/ircii">ircII</a>,
<a href="https://packages.debian.org/stable/net/irssi">irssi</a>,
<a href="https://packages.debian.org/stable/net/epic5">epic5</a> и
<a href="https://packages.debian.org/stable/net/kvirc">KVIrc</a>
&mdash; одни из самых популярных клиентов для
Unix и все они уже существуют в виде пакетов в
Debian. Кроме того, OFTC предлагает <a href="https://www.oftc.net/WebChat/">WebChat</a>,
веб-интерфейс, позволяющей подключаться к IRC с помощью браузера, не
устанавливая клиентскую программу на локальном компьютере.</p>

<p>Теперь когда у Вас есть клиент, Вам необходимо сказать ему, чтобы он
соединился с irc-сервером <code>irc.debian.org</code>.
В большинстве клиентов вы должны выполнить для этого следующую команду:

<pre>
/server irc.debian.org
</pre>

<p>В некоторых клиентах (например, irssi) вам следует ввести следующую команду:</p>

<pre>
/connect irc.debian.org
</pre>

# Note to translators:
# You might want to insert here a paragraph stating which IRC channel is available
# for user support in your language and pointing to the English IRC channel.
# <p>Once you are connected, join channel <code>#debian-foo</code> by typing</p>
# <pre>/join #debian</pre>
# for support in your language.
# <p>For support in English, read on</p>

<p>Когда вы соединились, войдите в канал <code>#debian</code>, введя</p>

<pre>
/join #debian
</pre>

<p>Замечание: обратите внимание, что клиенты, подобные HexChat, предлагают
графический интерфейс подключения к серверам и каналам.</p>

<p>Теперь вы оказались среди дружественной толпы обитателей канала
<code>#debian</code>. Здесь вы можете задавать вопросы по Debian.
FAQ канала можно найти по адресу
<url "https://wiki.debian.org/DebianIRC">.</p>


<p>Есть и другие сети IRC, где вы также можете говорить о Debian. Одна
из наиболее известных <a href="https://freenode.net/">IRC-сети freenode</a> на
<kbd>chat.freenode.net</kbd>.</p>


<toc-add-entry name="mail_lists" href="MailingLists/">Списки рассылки</toc-add-entry>

<p>Debian разрабатывается распределённо по всему миру. Поэтому
предпочтительный способ обсуждения различных вопросов&nbsp;&mdash;
электронная почта. Большая часть переговоров между разработчиками и
пользователями Debian происходит через списки рассылки.</p>

<p>Есть несколько общедоступных списков рассылки. Подробности смотрите на странице
<q><a href="MailingLists/">списки рассылки Debian</a></q>.</p>

# Note to translators:
# You might want to adapt the following paragraph, stating which list
# is available for user support in your language instead of English.
<p>
Поддержка пользователей на русском языке осуществляется в
списке рассылки
<a href="https://lists.debian.org/debian-russian/">debian-russian</a>.
</p>

<p>
Для поддержки пользователей на других языках также существуют свои списки рассылки. Подробности смотрите на странице
<a href="https://lists.debian.org/users.html">перечень
списков рассылки для пользователей</a>.
</p>

<p>Существует также множество других списков рассылки, посвящённых различным
аспектам работы в Linux, но не относящихся непосредственно к Debian.
С помощью вашей любимой поисквой машины вы сможете найти наиболее
подходящий под конкретную задачу.</p>

<toc-add-entry name="usenet">Конференции Usenet</toc-add-entry>

<p>Многие наши списки рассылки <a href="#mail_lists">mailing lists</a>
также доступны как конференции в иерархии <kbd>linux.debian.*</kbd>.
Также их можно просматривать и через веб-интерфейс при помощи
<a href="https://groups.google.com/forum/">Google Groups</a>.</p>

<toc-add-entry name="web">Веб-сайты</toc-add-entry>

<h3 id="forums">Форумы</h3>

# Note to translators:
# If there is a specific Debian forum for your language you might want to
# insert here a paragraph stating which list is available for user support
# in your language and pointing to the English forums.
# <p><a href="http://someforum.example.org/">someforum</a> is a web portal
# on which you can use your language to discuss Debian-related topics,
# submit questions about Debian, and have them answered by other users.</p>
#
# <p><a href="http://forums.debian.net">Debian User Forums</a> is a web portal
# on which you can use the English language to discuss Debian-related topics,
# submit questions about Debian, and have them answered by other users.</p>

<p><a href="http://debianforum.ru/">debianforum.ru</a> &nbsp;&mdash; это
форум русскоязычного сообщества Debian.</p>

<p>На форумах <a href="http://unixforum.org/index.php?showforum=13">unixforum.org</a> и <a
href="http://linuxforum.ru/viewforum.php?id=6">linuxforum.ru</a> присутствуют разделы,
посвящённые Debian.</p>

<p><a href="http://forums.debian.net">Debian User Forums</a> &nbsp;&mdash; это
веб-портал, на котором вы можете обсудить темы, касающиеся Debian, задать вопросы о Debian, и получить на них
ответы от других пользователей.</p>

<toc-add-entry name="maintainers">Как связаться с сопровождающим пакета</toc-add-entry>

<p>Связаться с сопровождающими пакета можно двумя способами. Если вы хотите
сообщить об ошибке в пакете, просто создайте сообщение об ошибке (см.
ниже раздел Система отслеживания ошибок). Сопровождающий получит копию
сообщения.</p>

<p>Если вы просто хотите пообщаться с сопровождающим, вы можете использовать
специальные почтовые псевдонимы, создаваемые для всех пакетов. Любое
сообщение, посланное по адресу &lt;<em>имя пакета</em>&gt;@packages.debian.org
будет переслано сопровождающему, ответственному за этот пакет.</p>


<toc-add-entry name="bts" href="Bugs/">Система отслеживания ошибок</toc-add-entry>

<p>Дистрибутив Debian имеет систему отслеживания ошибок,
содержащую подробную информацию об ошибках, замеченных пользователями
и разработчиками. Каждая ошибка получает свой номер и остаётся в базе,
пока не будет помечена как исправленная.</p>

<p>Чтобы сообщить об обнаруженной ошибке, вы можете использовать одну из
перечисленных ниже страниц ошибок либо воспользоваться пакетом <q>reportbug</q> для
автоматического создания сообщения.</p>

<p>Информацию о посылке сообщений об ошибках, просмотре незакрытых
ошибок и системе отслеживания ошибок в целом можно найти на страницах
<a href="Bugs/">системы отслеживания ошибок</a>.</p>


<toc-add-entry name="consultants" href="consultants/">Консультанты</toc-add-entry>

<p>Debian&nbsp;&mdash; это свободное программное обеспечение и предлагает
свободную поддержку через списки рассылки. Но некоторые люди не имеют времени
либо имеют специфические потребности, и они желают нанять кого-нибудь для
сопровождения или добавления дополнительной функциональности их системе
на основе Debian. См. список таких людей и компаний на странице
<a href="consultants/">консультантов</a>.</p>


<toc-add-entry name="release" href="releases/stable/">Известные проблемы</toc-add-entry>

<p>Ограничения и серьёзные проблемы текущего стабильного выпуска
(если таковые имеются) описаны на <a href="releases/stable/">страницах выпуска</a>.</p>

<p>Особое внимание следует уделить <a href="releases/stable/releasenotes">информации
о выпуске</a> и the <a href="releases/stable/errata">информации об известных ошибках</a>.</p>
