# translation of templates.po to Dutch
# Templates files for webwml modules
# Copyright (C) 2003,2004 Software in the Public Interest, Inc.
#
#
# Frans Pop <elendil@planet.nl>, 2007, 2008.
# Jeroen Schot <schot@a-eskwadraat.nl>, 2011.
# Frans Spiesschaert <Frans.Spiesschaert@yucom.be>, 2014.
msgid ""
msgstr ""
"Project-Id-Version: templates\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2014-09-10 13:00+0200\n"
"Last-Translator: Frans Spiesschaert <Frans.Spiesschaert@yucom.be>\n"
"Language-Team: Dutch <debian-l10n-dutch@lists.debian.org>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 1.4\n"

#. note: only change the sep(arator) if it's not good for your charset
#: ../../english/template/debian/cdimage.wml:10
msgid "&middot;"
msgstr "&middot;"

#: ../../english/template/debian/cdimage.wml:13
msgid "<void id=\"dc_faq\" />FAQ"
msgstr "<void id=\"dc_faq\" />FAQ"

#: ../../english/template/debian/cdimage.wml:16
msgid "Download with Jigdo"
msgstr "Downloaden met Jigdo"

#: ../../english/template/debian/cdimage.wml:19
msgid "Download via HTTP/FTP"
msgstr "Downloaden met HTTP/FTP"

#: ../../english/template/debian/cdimage.wml:22
msgid "Buy CDs or DVDs"
msgstr "Koop CD's of DVD's"

#: ../../english/template/debian/cdimage.wml:25
msgid "Network Install"
msgstr "Netwerkinstallatie"

#: ../../english/template/debian/cdimage.wml:28
msgid "<void id=\"dc_download\" />Download"
msgstr "<void id=\"dc_download\" />Download"

#: ../../english/template/debian/cdimage.wml:31
msgid "<void id=\"dc_misc\" />Misc"
msgstr "<void id=\"dc_misc\" />Diversen"

#: ../../english/template/debian/cdimage.wml:34
msgid "<void id=\"dc_artwork\" />Artwork"
msgstr "<void id=\"dc_artwork\" />Kunst"

#: ../../english/template/debian/cdimage.wml:37
msgid "<void id=\"dc_mirroring\" />Mirroring"
msgstr "<void id=\"dc_mirroring\" />Spiegelserver"

#: ../../english/template/debian/cdimage.wml:40
msgid "<void id=\"dc_rsyncmirrors\" />Rsync Mirrors"
msgstr "<void id=\"dc_rsyncmirrors\" />Rsync-spiegelservers"

#: ../../english/template/debian/cdimage.wml:43
msgid "<void id=\"dc_verify\" />Verify"
msgstr "<void id=\"dc_verify\" />Verifiëren"

#: ../../english/template/debian/cdimage.wml:46
msgid "<void id=\"dc_torrent\" />Download with Torrent"
msgstr "<void id=\"dc_torrent\" />Download met Torrent"

#: ../../english/template/debian/cdimage.wml:49
msgid "<void id=\"dc_relinfo\" />Image Release Info"
msgstr "<void id=\"dc_relinfo\" />Image Release-informatie"

#: ../../english/template/debian/cdimage.wml:52
msgid "Debian CD team"
msgstr "Debian CD-team"

#: ../../english/template/debian/cdimage.wml:55
msgid "debian_on_cd"
msgstr "Debian op CD"

#: ../../english/template/debian/cdimage.wml:58
msgid "<void id=\"faq-bottom\" />faq"
msgstr "<void id=\"faq-bottom\" />FAQ"

#: ../../english/template/debian/cdimage.wml:61
msgid "jigdo"
msgstr "Jigdo"

#: ../../english/template/debian/cdimage.wml:64
msgid "http_ftp"
msgstr "HTTP/FTP"

#: ../../english/template/debian/cdimage.wml:67
msgid "buy"
msgstr "Koop"

#: ../../english/template/debian/cdimage.wml:70
msgid "net_install"
msgstr "Netwerkinstallatie"

#: ../../english/template/debian/cdimage.wml:73
msgid "<void id=\"misc-bottom\" />misc"
msgstr "<void id=\"misc-bottom\" />Diversen"

#: ../../english/template/debian/cdimage.wml:76
msgid ""
"English-language <a href=\"/MailingLists/disclaimer\">public mailing list</"
"a> for CDs/DVDs:"
msgstr ""
"Engelstalige <a href=\"/MailingLists/disclaimer\">publieke mailinglijst</a> "
"voor CD's/DVD's"

#. Translators: string printed by gpg --fingerprint in your language, including spaces
#: ../../english/CD/CD-keys.data:4 ../../english/CD/CD-keys.data:8
#: ../../english/CD/CD-keys.data:13
msgid "      Key fingerprint"
msgstr "      Vingerafdruk van de Sleutel"

#: ../../english/devel/debian-installer/images.data:87
msgid "ISO images"
msgstr "ISO-images"

#: ../../english/devel/debian-installer/images.data:88
msgid "Jigdo files"
msgstr "Jigdo-bestanden"
