#use wml::debian::template title="Pakketten die werk nodig hebben en toekomstige pakketten"
#use wml::debian::toc
#include "$(ENGLISHDIR)/devel/wnpp/wnpp.data"
#use wml::debian::translation-check translation="8899b1fc22972af8b453b79a53a791a837a26f10"

<define-tag toc-title-formatting endtag=required><h3>%body</h3></define-tag>

<p>Pakketten die werk nodig hebben en toekomstige pakketten (Work-Needing
and Prospective Packages, afgekort WNPP) is een lijst van pakketten in
Debian die een nieuwe onderhouder nodig hebben, en van toekomstige
pakketten.
Om de werkelijke status van dergelijke zaken op de voet te volgen, wordt
WNPP momenteel als een pseudo-pakket behandeld in het
<a href="https://bugs.debian.org/">Debian Bug Tracking System (BTS)</a>,
het bugvolgsysteem van Debian.</p>

<hrline />

<p><a href="work_needing">Pakketten die een nieuwe onderhouder nodig hebben</a>:
</p>
<ul>
  <li>
      <a href="rfa_bypackage"><rfa_number> pakketten die klaar voor
      adoptie zijn</a>,
      gerangschikt <a href="rfa_bymaint">volgens onderhouder</a>
      of <a href="rfa_byage">volgens ouderdom</a>
  </li>
  <li>
      <a href="orphaned"><orphaned_number> verweesde pakketten</a>,
      gerangschikt <a href="orphaned_byage">volgens ouderdom</a>
  </li>
  <li>
      <a href="being_adopted"><adopted_number> pakketten die momenteel
      geadopteerd worden</a>,
      gerangschikt <a href="being_adopted_byage">volgens ouderdom</a>
      of <a href="being_adopted_byactivity">volgens activiteit</a>
  </li>
</ul>

<p>
<a href="help_requested"><help_number> pakketten die hulp nodig hebben</a>,
gerangschikt <a href="help_requested_byage">volgens ouderdom</a>
of <a href="help_requested_bypop">volgens populariteit</a>
</p>

<p><a href="prospective">Toekomstige pakketten</a>:</p>
<ul>
  <li>
      <a href="being_packaged"><packaged_number> pakketten waaraan gewerkt
      wordt</a>,
      gerangschikt <a href="being_packaged_byage">volgens ouderdom</a>
      of <a href="being_packaged_byactivity">volgens activiteit</a>
  </li>
  <li>
      <a href="requested"><requested_number> aangevraagde pakketten</a>,
      gerangschikt <a href="requested_byage">volgens ouderdom</a>
  </li>
</ul>


<p><a href="unable-to-package">Software die niet verpakt kan worden</a></p>

<p>Opmerking: deze lijsten worden zesmaal daags bijgewerkt; raadpleeg voor
meer actuele informatie het item in het BTS over het
<a href="https://bugs.debian.org/wnpp">pseudo-pakket wnpp</a>.</p>

<p>Op de <a href="https://wnpp.debian.net">WNPP-zoekpagina</a> kunt
u bovenstaande informatie doorzoeken op pakket, beschrijving of type.</p>

<p>U kunt de bovenstaande informatie, opgedeeld in verschillende
categorieën (op basis van <a href="https://debtags.debian.org/">debtags</a>), doorbladeren
op de webpagina <a href="https://wnpp-by-tags.debian.net">WNPP-by-tags</a>.</p>

<hrline />

<h2>WNPP gebruiken</h2>

<toc-display />

<p>Aangezien gebruik gemaakt wordt van het BTS, is iedere ontwikkelaar
reeds vertrouwd met de technische details, zoals het indienen van nieuwe
informatie, het aanpassen van informatie of het sluiten van openstaande
verzoeken. Om anderzijds een zo hoog mogelijk niveau van automatisering
mogelijk te maken, dienen sommige procedures in acht genomen te worden.</p>

<p>Om nieuwe informatie in te dienen moet voor elk betrokken (toekomstig)
pakket een bugrapport ingediend worden tegen het
<a href="https://bugs.debian.org/wnpp">pseudo-pakket wnpp</a>.
Merk op dat u slecht één bugrapport moet indienen per broncodepakket
en niet voor elk binair pakket dat uit het broncodepakket gecompileerd
wordt.</p>


<toc-add-entry>Nieuwe items toevoegen met <q>reportbug</q></toc-add-entry>

<p>U kunt gebruik maken van <a href="https://packages.debian.org/reportbug">reportbug</a>
(<kbd>apt-get install reportbug</kbd>):</p>

<samp>
  $ reportbug --email <var>gebruikersnaam@domein.tld</var> wnpp<br />
  Gebruik '<var>Uw naam &lt;gebruikersnaam@domein.tld&gt;</var>' als
  afzenderadres.<br />
  Status voor wnpp ophalen...<br />
  Bugvolgsysteem wordt doorzocht op rapporten over wnpp<br />
  (Gebruik aan de prompt ? voor hulp.)<br />
  ...<br />
</samp>

<p>U zult een lijst te zien krijgen van gerapporteerde bugs tegen WNPP.
Die zou u moeten lezen om een tweede rapport voor hetzelfde pakket te
voorkomen.</p>
<p>Na de buglijst zal gevraagd worden naar het type verzoek:</p>

<samp>
Welk soort verzoek is dit?<br />
<br />
1 ITP  Dit is een <q>Intent To Package</q> (verpakkingsintentie). Dien<br />
       bij een dergelijk rapport een pakketbeschrijving in,<br />
       samen met copyrightinformatie en URL.<br />
<br />
2 O    Het pakket werd verweesd: <q>Orphaned</q>. Het heeft zo snel<br />
       mogelijk een nieuwe onderhouder nodig.<br />
<br />
3 RFA  Dit is een <q>Request for Adoption</q> (adoptieverzoek). Door<br />
       te weinig tijd, middelen, interesse of iets dergelijks vraagt<br />
       de huidige onderhouder iemand anders om dit pakket te<br />
       onderhouden.<br />
       In afwachting zal deze het zelf onderhouden, maar misschien<br />
       niet op de best mogelijke manier.<br />
       Kort gezegd: het pakket heeft een nieuwe onderhouder nodig.<br />
<br />
4 RFH  Dit is een <q>Request For Help</q> (vraag om hulp). De huidige<br />
       onderhouder wenst het pakket verder te onderhouden, maar hij/zij<br />
       heeft hiervoor wat hulp nodig, omdat zijn/haar tijd beperkt is<br />
       of omdat het pakket behoorlijk groot is en meerdere<br />
       onderhouders nodig heeft.<br />
<br />
5 RFP  Dit is een <q>Request For Package</q> (verpakkingsverzoek).<br />
       U vond een interessant stukje software en zou graag hebben dat<br />
       iemand anders het voor Debian zou onderhouden. Voeg in een<br />
       dergelijk rapport een pakketbeschrijving toe,<br />
       samen met copyrightinformatie en URL.<br />
<br />
Kies het type verzoek: <br />
</samp>

<p>Na uw selectie zal u naar de naam van het pakket gevraagd worden:</p>

<samp>
Kies het type verzoek: <var>x</var><br />
Voer de voorgestelde pakketnaam in: <var>PAKKETNAAM</var><br />
De status van de database wordt gecontroleerd...<br />
</samp>

<ul>
<li><p>Indien uw type verzoek ITP (1) of RFP (4) is, zal u om een korte
   beschrijving gevraagd worden en vervolgens wat informatie over het
   pakket:</p>

<samp>
Beschrijf kort dit pakket; dit moet een passende korte beschrijving zijn
voor het gebeurlijke pakket:<br />
&gt; <var>EEN BESCHRIJVING</var><br />
<br />
Onderwerp: ITP: <var>PAKKETNAAM -- EEN BESCHRIJVING</var><br />
Pakket: wnpp<br />
Versie: n.v.t.; gerapporteerd op 30-01-2002<br />
Ernstigheid: wensenlijst<br />
<br />
* Pakketnaam           : <var>PAKKETNAAM</var><br />
  Versie               : <var>x.y.z</var><br />
  Bovenstroomse auteur : <var>Naam &lt;iemand@een.org&gt;</var><br />
* URL                  : <var>http://www.een.org/</var><br />
* Licentie             : <var>(GPL, LGPL, BSD, MIT/X, enz.)</var><br />
  Beschrijving         : <var>EEN BESCHRIJVING</var><br />
<br />
<br />
-- Systeeminformatie<br />
...<br />
</samp>

<p>Onder de regel met de <q>Beschrijving</q> moet u meer informatie over
het pakket geven.</p></li>

<li><p>Indien uw type verzoek O (2) of RFA (3) is, moet u de pakketnaam
opgeven.</p>

<samp>
Kies het type verzoek: <var>x</var><br />
Voer de naam van het pakket in: <var>PAKKETNAAM</var><br />
De status van de database wordt gecontroleerd...<br />
<br />
Onderwerp: O: <var>PAKKETNAAM -- KORTE BESCHRIJVING</var><br />
Pakket: wnpp<br />
Versie: n.v.t.; gerapporteerd op 30-01-2002<br />
Ernstigheid: normaal<br />
<br />
<br />
<br />
-- Systeeminformatie<br />
...<br />
</samp>

<p>U moet wat informatie toevoegen over het onderhoud van het pakket, over
de bovenstroomse situatie en misschien een reden waarom u het pakket wenst
weg te geven.</p></li>

</ul>

<p>Daarna wordt u gevraagd of u uw verzoek wenst te verzenden:</p>

<samp>
Het rapport zal verzonden worden naar het Debian Bug Tracking System - bugvolgsysteem &lt;submit@bugs.debian.org&gt;<br />
Dit bugrapport indienen (e om te editten) [Y|n|i|e|?]? <br />
</samp>


<toc-add-entry>Nieuwe items toevoegen via e-mail</toc-add-entry>

<p>Het is ook mogelijk om via e-mail rapporten/bugs te rapporteren tegen
WNPP. De indeling van de inzending moet er zo uitzien:</p>

<samp>
  Aan: submit@bugs.debian.org<br />
  Onderwerp: <var>TAG</var>: <var>pakketnaam</var> -- <var>korte
  pakketbeschrijving</var><br />
  <br />
  Pakket: wnpp<br />
  Ernstigheid: <var>zie hieronder</var><br />
  <br />
  <var>Wat informatie over het pakket.</var>  (Als het een ITP of RFP
  betreft, is een URL waar het pakket (ofwel het .deb of de originele
  broncode) gedownload kan worden vereist, evenals informatie over zijn
  licentie.)
</samp>

<p>De te gebruiken tags en hun respectieve ernstigheid zijn:</p>

<table>
<colgroup span="3">
<col width="10%">
<col width="10%">
<col width="80%">
</colgroup>
<tr>
  <th valign="top"><a name="tag-o">O</a></th>
  <td valign="top"><em>normaal</em></td>
  <td>Het pakket werd <q>Orphaned</q> (verweesd). Het heeft zo spoedig
      mogelijk een nieuwe onderhouder nodig. Indien de prioriteit van
      het pakket standaard of hoger is, dan moet important (belangrijk)
      als ernstigheid gekozen worden.
  </td>
</tr>
<tr>
  <th valign="top"><a name="tag-rfa">RFA</a></th>
  <td valign="top"><em>normaal</em></td>
  <td>Dit is een <q>Request for Adoption</q> (adoptieverzoek). Door te
      weinig tijd, middelen, interesse of iets dergelijks, vraagt de
      huidige onderhouder dat dit pakket door iemand anders onderhouden
      zou worden. Intussen blijft hij/zij het verder onderhouden,
      misschien niet op de best mogelijke wijze. Kort gezegd: het pakket
      heeft een nieuwe onderhouder nodig.
  </td>
</tr>
<tr>
  <th valign="top"><a name="tag-rfh">RFH</a></th>
  <td valign="top"><em>normaal</em></td>
  <td>Dit is een <q>Request For Help</q> (vraag om hulp). De
      huidige onderhouder wenst dit pakket te blijven onderhouden, maar
      heeft hierbij wat hulp nodig, door beperkte beschikbare tijd of
      omdat het pakket behoorlijk groot is en meerdere onderhouders nodig
      heeft.
  </td>
</tr>
<tr>
  <th valign="top"><a name="tag-itp">ITP</a></th>
  <td valign="top"><em>wensenlijst</em></td>
  <td>Dit is een <q>Intent To Package</q> (verpakkingsintentie). Dien bij
      een dergelijk rapport een pakketbeschrijving in,
      samen met copyrightinformatie en een URL.
  </td>
</tr>
<tr>
  <th valign="top"><a name="tag-rfp">RFP</a></th>
  <td valign="top"><em>wensenlijst</em></td>
  <td>Dit is een <q>Request For Package</q> (verpakkingsverzoek). Iemand
      vond een interessant stukje software en zou graag hebben dat iemand
      anders het voor Debian zou onderhouden. Dien bij een dergelijk
      rapport een pakketbeschrijving in, samen met copyrightinformatie en
      een URL.
  </td>
</tr>
</table>


<toc-add-entry>Items verwijderen</toc-add-entry>

<p>De werkwijze voor het sluiten van deze bugs is de volgende:
</p>

<table>
<colgroup span="2">
<col width="10%">
<col width="90%">
</colgroup>
<tr>
  <th valign="top"><a name="howto-o">O</a></th>
  <td>Indien u een pakket gaat adopteren, moet u de bug ervan hernoemen
      door <q>O</q> te vervangen door <q>ITA</q>, opdat anderen zouden
      weten dat het pakket geadopteerd wordt, om te voorkomen dat het
      automatisch verwijderd zou worden en om uzelf als bugeigenaar in te
      stellen.
      Om het pakket effectief te adopteren, moet u het uploaden met uw naam
      in het veld Maintainer:.
      En in het changelog-bestand van het pakket voegt u iets toe in de
      zin van
      <code>
      * New maintainer (Closes: #<var>bugnummer</var>)
      </code>,
      opdat deze bug automatisch gesloten zou worden, eens het pakket
      geïnstalleerd is. Daarbij vervangt u <var>bugnumber</var> door
      het nummer van het overeenkomstige bugrapport.
      Vooraleer u het pakket uploadt moet u nagaan of er bovenstrooms
      een eventuele nieuwe release is en zou u de openstaande bugs
      moeten proberen repareren.
  </td>
</tr>
<tr>
  <th valign="top"><a name="howto-rfa">RFA</a></th>
  <td><p>Indien u een pakket gaat adopteren, moet u de bug ervan hernoemen
      door <q>RFA</q> te vervangen door <q>ITA</q>, opdat anderen zouden
      weten dat het pakket geadopteerd wordt, om te voorkomen dat het
      automatisch verwijderd zou worden en om uzelf als bugeigenaar in te
      stellen.
      Om het pakket effectief te adopteren, moet u het uploaden met uw naam
      in het veld Maintainer: en sluit u deze bug nadat het pakket
      geïnstalleerd werd.
      </p>

      <p>Indien u als pakketonderhouder beslist een pakket dat u als
      <q>RFA</q> gemarkeerd had, tot wees te maken, hernoem dan het
      bugrapport en vervang <q>RFA</q> door <q>O</q>. Indien u uw verzoek
      intrekt, sluit dan de bug.</p>
  </td>
</tr>
<tr>
  <th valign="top"><a name="howto-rfh">RFH</a></th>
  <td><p>Normaal gezien zou enkel de indiener, d.w.z. de pakketonderhouder,
      deze bug mogen sluiten wanneer die de bug als verouderd beschouwt,
      omdat één of meer mensen hulp aangeboden (en geboden) hebben, of
      omdat de indiener het pakket nu zelf denkt aan te kunnen.
      </p>

      <p>Indien u als pakketonderhouder beslist om een RFH te veranderen
      naar een adoptieverzoek (<q>RFA</q>) of indien u het pakket tot wees
      wil maken (<q>O</q>), hernoem dan de bug in plaats van deze te
      sluiten en een nieuw bugrapport in te dienen.</p>
  </td>
</tr>
<tr>
  <th valign="top"><a name="howto-itp">ITP</a></th>
  <td><p>verpak de software, upload deze en sluit deze bug wanneer het
      pakket geïnstalleerd is.
      </p>

      <p>Indien u van mening verandert en dit niet langer wenst te
      verpakken, sluit deze bug dan of hernoem ze en herklasseer ze als
      RFP, naargelang wat u het meest passend vindt.</p>

	  <p>Indien u problemen ervaart bij het verpakken van het programma
      (bijvoorbeeld omdat het een ander, nog niet verpakt programma
      vereist, waarvoor u zelf niet de nodige tijd heeft om het te
      verpakken), zou u deze problemen eventueel als bijkomende informatie
      kunnen vermelden in het ITP, zodat het duidelijk is hoe het staat
      met uw verpakkingsinspanningen.</p>
  </td>
</tr>
<tr>
  <th valign="top"><a name="howto-rfp">RFP</a></th>
  <td>Indien u dit gaat verpakken, hernoem het bugrapport dan door
      <q>RFP</q> te vervangen door <q>ITP</q>, zodat andere mensen weten
      dat het programma reeds verpakt wordt, en stel uzelf in als eigenaar
      van deze bug. Verpak nadien de software, upload ze en sluit de bug
      wanneer het pakket geïnstalleerd is.
  </td>
</tr>
</table>

<p>Indien u meent dat de mailinglijst voor ontwikkelaars op de hoogte moet
zijn van uw ITP, RFA of iets anders, voeg dan de kopregel</p>
<pre>X-Debbugs-CC: debian-devel@lists.debian.org</pre>
<p>toe aan het bericht.</p>

<p>De makkelijkste manier om deze bugs te sluiten, is in het
changelog-bestand van het pakket een item toevoegen waarin u vermeldt wat
u gedaan heeft en er <tt>(closes:&nbsp;bug#nnnnn)</tt> aan toevoegen. Op
die manier zal de bug automatisch gesloten worden nadat het pakket in het
archief geïnstalleerd werd.</p>

<p>
<strong>Opgelet:</strong> Indien u een bug opnieuw moet toewijzen, moet
hernoemen of de eigenaar ervan moet instellen, moet u dit doen door
<a href="$(HOME)/Bugs/server-control">rechtstreeks</a> te e-mailen naar
de bot die het BTS aanstuurt of door het rapportnummer @bugs.debian.org te
e-mailen en <a href="$(HOME)/Bugs/Reporting#control">control-pseudo-headers</a> te gebruiken, maar <strong>niet</strong> door een nieuw
bugrapport in te sturen.
</p>

<p>Opmerking: als een pakket voor een heel lange tijd verweesd blijft, zullen we de situatie onderzoeken om uit te maken of het pakket nog nodig is &mdash; als dat niet het geval is zal de FTP-onderhouders gevraagd worden om het pakket uit unstable te verwijderen.</p>

<p>Indien u om een af andere reden de WNPP-onderhouders moet
contacteren, zijn deze te bereiken op <email wnpp@debian.org>.</p>
