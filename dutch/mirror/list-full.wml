#use wml::debian::template title="Debian wereldwijde spiegelservers" BARETITLE=true
#use wml::debian::translation-check translation="346f82cf48a105b4686a44b1d3a4fe7a11adc9c0"

<p>Dit is een <strong>volledige</strong> lijst van spiegelservers van Debian.
Voor elke site wordt vermeld welke types materiaal beschikbaar zijn, samen
met de toegangsmethode voor elk type.</p>

<p>De volgende zaken worden gespiegeld:</p>

<dl>
<dt><strong>Pakketten</strong></dt>
	<dd>De Debian-pakketpool.</dd>
<dt><strong>cd-images</strong></dt>
	<dd>Officiële Debian cd-images. Zie
	<url "https://www.debian.org/CD/"> voor details.</dd>
<dt><strong>Oude releases</strong></dt>
	<dd>Het archief van oude uitgebrachte versies van Debian.
	<br />
	Sommige van de oude uitgaven bevatten ook het zogenaamde debian-non-US
	archief, met afdelingen met Debian pakketten welke niet in de US
	konden verspreid worden door softwarepatenten of omdat encryptie
    gebruikt wordt.
	De updates van debian-non-US werden stopgezet met Debian 3.1.</dd>

</dl>

<p>De volgende toegangsmethodes zijn mogelijk:</p>

<dl>
<dt><strong>HTTP</strong></dt>
	<dd>Standaard webtoegang, maar kan gebruikt worden om bestanden te downloaden.</dd>
<dt><strong>rsync</strong></dt>
	<dd>Een efficiënt middel om te spiegelen.</dd>
</dl>

<p>Het element <q>Type</q> kan een van de volgende zijn:</p>

<dl>
<dt><strong>leaf</strong></dt>
	<dd>Deze omvatten het grootste deel van de spiegelservers.</dd>
<dt><strong>Push-Secondary</strong></dt>
	<dd>Deze sites spiegelen rechtstreeks van een Push-Primary site en
    gebruiken push-spiegelen.</dd>
<dt><strong>Push-Primary</strong></dt>
	<dd>Deze sites spiegelen rechtstreeks van de hoofdarchiefsite (die
	niet publiek toegankelijk is) en gebruiken push-spiegelen.</dd>
</dl>

<p>(Zie <a href="https://www.debian.org/mirror/push_mirroring">de pagina over
 push-spiegelen</a> voor details hierover.)</p>

<p>Het gezaghebbende exemplaar van de volgende lijst kan altijd gevonden
worden op: <url "https://www.debian.org/mirror/list-full">.
<br />
Al het overige dat u wenst te weten over Debian spiegelservers:
<url "https://www.debian.org/mirror/">.
</p>

<hr style="height:1">

<p>Spring rechtstreeks naar een land in de lijst:
<br />

#include "$(ENGLISHDIR)/mirror/list-full.inc"
