# Debian Web Site. templates.it.po
# Copyright (C) 2002 Giuseppe Sacco.
# Giuseppe Sacco <eppesuigoccas@libero.it>, 2002.
# Giuseppe Sacco <eppesuig@debian.org>, 2004-2007.
# Luca Monducci <luca.mo@tiscali.it>, 2013.
#
msgid ""
msgstr ""
"Project-Id-Version: templates.it\n"
"PO-Revision-Date: 2013-03-03 15:34+0100\n"
"Last-Translator: Luca Monducci <luca.mo@tiscali.it>\n"
"Language-Team: debian-l10n-italian <debian-l10n-italian@lists.debian.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/template/debian/consultant.wml:6
msgid "List of Consultants"
msgstr "Elenco dei consulenti"

#: ../../english/template/debian/consultant.wml:9
msgid "Back to the <a href=\"./\">Debian consultants page</a>."
msgstr "Ritorna alla <a href=\"./\">pagina dei consulenti Debian</a>."

#: ../../english/consultants/consultant.defs:6
msgid "Name:"
msgstr "Nome:"

#: ../../english/consultants/consultant.defs:9
msgid "Company:"
msgstr "Azienda:"

#: ../../english/consultants/consultant.defs:12
msgid "Address:"
msgstr "Indirizzo:"

#: ../../english/consultants/consultant.defs:15
msgid "Contact:"
msgstr "Contatti:"

#: ../../english/consultants/consultant.defs:19
msgid "Phone:"
msgstr "Telefono:"

#: ../../english/consultants/consultant.defs:22
msgid "Fax:"
msgstr "Fax:"

#: ../../english/consultants/consultant.defs:25
msgid "URL:"
msgstr "URL:"

#: ../../english/consultants/consultant.defs:29
msgid "or"
msgstr "o"

#: ../../english/consultants/consultant.defs:34
msgid "Email:"
msgstr "E-mail:"

#: ../../english/consultants/consultant.defs:52
msgid "Rates:"
msgstr "Tariffe:"

#: ../../english/consultants/consultant.defs:55
msgid "Additional Information"
msgstr "Informazioni aggiuntive"

#: ../../english/consultants/consultant.defs:58
msgid "Willing to Relocate"
msgstr "Disponibile alla trasferta"

#: ../../english/consultants/consultant.defs:61
msgid ""
"<total_consultant> Debian consultants listed in <total_country> countries "
"worldwide."
msgstr ""
"<total_consultant> consulenti Debian elencati in <total_country> paesi."
