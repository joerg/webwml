# bugs webwml Catalan template.
# Copyright (C) 2002, 2003, 2004, 2005 Free Software Foundation, Inc.
# Jordi Mallach <jordi@debian.org>, 2002, 2003.
# Guillem Jover <guillem@debian.org>, 2004, 2005, 2007, 2009, 2011, 2017-2018.
#
msgid ""
msgstr ""
"Project-Id-Version: webwml\n"
"PO-Revision-Date: 2018-07-12 00:56+0200\n"
"Last-Translator: Guillem Jover <guillem@debian.org>\n"
"Language-Team: Catalan <debian-l10n-catalan@lists.debian.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/Bugs/pkgreport-opts.inc:17
msgid "in package"
msgstr "en el paquet"

#: ../../english/Bugs/pkgreport-opts.inc:20
#: ../../english/Bugs/pkgreport-opts.inc:60
#: ../../english/Bugs/pkgreport-opts.inc:94
msgid "tagged"
msgstr "etiquetat"

#: ../../english/Bugs/pkgreport-opts.inc:23
#: ../../english/Bugs/pkgreport-opts.inc:63
#: ../../english/Bugs/pkgreport-opts.inc:97
msgid "with severity"
msgstr "amb gravetat"

#: ../../english/Bugs/pkgreport-opts.inc:26
msgid "in source package"
msgstr "en el paquet font"

#: ../../english/Bugs/pkgreport-opts.inc:29
msgid "in packages maintained by"
msgstr "en el paquet mantingut per"

#: ../../english/Bugs/pkgreport-opts.inc:32
msgid "submitted by"
msgstr "enviat per"

#: ../../english/Bugs/pkgreport-opts.inc:35
msgid "owned by"
msgstr "el propietari és"

#: ../../english/Bugs/pkgreport-opts.inc:38
msgid "with status"
msgstr "amb estat"

#: ../../english/Bugs/pkgreport-opts.inc:41
msgid "with mail from"
msgstr "amb correu de"

#: ../../english/Bugs/pkgreport-opts.inc:44
msgid "newest bugs"
msgstr "informes d'error més nous"

#: ../../english/Bugs/pkgreport-opts.inc:57
#: ../../english/Bugs/pkgreport-opts.inc:91
msgid "with subject containing"
msgstr "amb assumpte que conté"

#: ../../english/Bugs/pkgreport-opts.inc:66
#: ../../english/Bugs/pkgreport-opts.inc:100
msgid "with pending state"
msgstr "amb estat pendent"

#: ../../english/Bugs/pkgreport-opts.inc:69
#: ../../english/Bugs/pkgreport-opts.inc:103
msgid "with submitter containing"
msgstr "amb remitent que conté"

#: ../../english/Bugs/pkgreport-opts.inc:72
#: ../../english/Bugs/pkgreport-opts.inc:106
msgid "with forwarded containing"
msgstr "amb reenviat que conté"

#: ../../english/Bugs/pkgreport-opts.inc:75
#: ../../english/Bugs/pkgreport-opts.inc:109
msgid "with owner containing"
msgstr "amb propietari que conté"

#: ../../english/Bugs/pkgreport-opts.inc:78
#: ../../english/Bugs/pkgreport-opts.inc:112
msgid "with package"
msgstr "amb paquet"

#: ../../english/Bugs/pkgreport-opts.inc:122
msgid "normal"
msgstr "normal"

#: ../../english/Bugs/pkgreport-opts.inc:125
msgid "oldview"
msgstr "vista antiga"

#: ../../english/Bugs/pkgreport-opts.inc:128
msgid "raw"
msgstr "cru"

#: ../../english/Bugs/pkgreport-opts.inc:131
msgid "age"
msgstr "antiguitat"

#: ../../english/Bugs/pkgreport-opts.inc:137
msgid "Repeat Merged"
msgstr "Repeteix fusionats"

#: ../../english/Bugs/pkgreport-opts.inc:138
msgid "Reverse Bugs"
msgstr "Invertir informes d'error"

#: ../../english/Bugs/pkgreport-opts.inc:139
msgid "Reverse Pending"
msgstr "Invertir pendents"

#: ../../english/Bugs/pkgreport-opts.inc:140
msgid "Reverse Severity"
msgstr "Invertir gravetat"

#: ../../english/Bugs/pkgreport-opts.inc:141
msgid "No Bugs which affect packages"
msgstr "No hi ha cap informe d'error que afecti algun paquet"

#: ../../english/Bugs/pkgreport-opts.inc:143
msgid "None"
msgstr "Cap"

#: ../../english/Bugs/pkgreport-opts.inc:144
msgid "testing"
msgstr "testing (en proves)"

#: ../../english/Bugs/pkgreport-opts.inc:145
msgid "oldstable"
msgstr "oldstable (antiga estable)"

#: ../../english/Bugs/pkgreport-opts.inc:146
msgid "stable"
msgstr "stable (estable)"

#: ../../english/Bugs/pkgreport-opts.inc:147
msgid "experimental"
msgstr "experimental"

#: ../../english/Bugs/pkgreport-opts.inc:148
msgid "unstable"
msgstr "unstable (inestable)"

#: ../../english/Bugs/pkgreport-opts.inc:152
msgid "Unarchived"
msgstr "Desarxivats"

#: ../../english/Bugs/pkgreport-opts.inc:155
msgid "Archived"
msgstr "Arxivats"

#: ../../english/Bugs/pkgreport-opts.inc:158
msgid "Archived and Unarchived"
msgstr "Arxivats i desarxivats"

#~ msgid "Flags:"
#~ msgstr "Opcions:"

#~ msgid "active bugs"
#~ msgstr "errors actius"

#~ msgid "display merged bugs only once"
#~ msgstr "mostra els errors fusionats només una vegada"

#~ msgid "no ordering by status or severity"
#~ msgstr "no ordena pel seu estat de gravetat"

#~ msgid "don't show table of contents in the header"
#~ msgstr "no ensenya taula de continguts a la capçalera"

#~ msgid "don't show statistics in the footer"
#~ msgstr "no ensenya estadístiques al peu de pàgina"

#~ msgid "proposed-updates"
#~ msgstr "proposed-updates (actualitzacions proposades)"

#~ msgid "testing-proposed-updates"
#~ msgstr "testing-proposed-updates (actualitzacions proposades de testing)"

#~ msgid "Package version:"
#~ msgstr "Versió del paquet:"

#~ msgid "Distribution:"
#~ msgstr "Distribució:"

#~ msgid "bugs"
#~ msgstr "errors"

#~ msgid "open"
#~ msgstr "obert"

#~ msgid "forwarded"
#~ msgstr "enviat"

#~ msgid "pending"
#~ msgstr "pendent"

#~ msgid "fixed"
#~ msgstr "fixat"

#~ msgid "done"
#~ msgstr "tancat"

#~ msgid "Include status:"
#~ msgstr "Inclou l'estat:"

#~ msgid "Exclude status:"
#~ msgstr "Exclou l'estat:"

#~ msgid "critical"
#~ msgstr "crític"

#~ msgid "grave"
#~ msgstr "greu"

#~ msgid "serious"
#~ msgstr "seriós"

#~ msgid "important"
#~ msgstr "important"

#~ msgid "minor"
#~ msgstr "menor"

#~ msgid "wishlist"
#~ msgstr "desig"

#~ msgid "Include severity:"
#~ msgstr "Inclou la gravetat:"

#~ msgid "Exclude severity:"
#~ msgstr "Exclou la gravetat:"

#~ msgid "potato"
#~ msgstr "potato"

#~ msgid "woody"
#~ msgstr "woody"

#~ msgid "sarge-ignore"
#~ msgstr "sarge-ignore"

#~ msgid "etch"
#~ msgstr "etch"

#~ msgid "etch-ignore"
#~ msgstr "etch-ignore"

#~ msgid "lenny"
#~ msgstr "lenny"

#~ msgid "lenny-ignore"
#~ msgstr "lenny-ignore"

#~ msgid "sid"
#~ msgstr "sid"

#~ msgid "confirmed"
#~ msgstr "confirmed (confirmat)"

#~ msgid "d-i"
#~ msgstr "d-i (instal·lador)"

#~ msgid "fixed-in-experimental"
#~ msgstr "fixed-in-experimental (arreglat a experimental)"

#~ msgid "fixed-upstream"
#~ msgstr "fixed-upstream (arreglat per l'autor original)"

#~ msgid "help"
#~ msgstr "help (ajuda)"

#~ msgid "l10n"
#~ msgstr "l10n (localització)"

#~ msgid "moreinfo"
#~ msgstr "moreinfo (més informació)"

#~ msgid "patch"
#~ msgstr "patch (pedaç)"

#~ msgid "security"
#~ msgstr "security (seguretat)"

#~ msgid "unreproducible"
#~ msgstr "unreproducible (no reproduïble)"

#~ msgid "upstream"
#~ msgstr "upstream (responsable autor original)"

#~ msgid "wontfix"
#~ msgstr "wontfix (no s'arreglarà)"

#~ msgid "ipv6"
#~ msgstr "ipv6"

#~ msgid "lfs"
#~ msgstr "lfs (sistema de fitxers gran)"

#~ msgid "Include tag:"
#~ msgstr "Inclou la etiqueta:"

#~ msgid "Exclude tag:"
#~ msgstr "Exclou la etiqueta:"
