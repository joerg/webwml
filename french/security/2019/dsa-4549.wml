#use wml::debian::translation-check translation="a7762d7ea18afc6d77cb26d372f94053b56575af" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Plusieurs problèmes de sécurité ont été découverts dans le navigateur
web Firefox de Mozilla, qui pourraient éventuellement avoir pour
conséquence l'exécution de code arbitraire, la divulgation d'informations,
un script intersite ou un déni de service.</p>

<p>Debian suit les éditions longue durée (ESR, « Extended Support
Release ») de Firefox. Le suivi des séries 60.x est terminé, aussi, à
partir de cette mise à jour, Debian suit les versions 68.x.</p>

<p>Pour la distribution oldstable (Stretch), quelques modifications de
configuration supplémentaires pour le réseau de serveurs d'empaquetage
s'avèrent nécessaires (pour fournir la nouvelle chaîne de compilation,
basée sur Rust, nécessaire à ESR68). Les paquets seront mis à disposition
quand ces modifications auront été résolues.</p>

<p>Pour la distribution stable (Buster), ces problèmes ont été corrigés
dans la version 68.2.0esr-1~deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets firefox-esr.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de firefox-esr,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/firefox-esr">\
https://security-tracker.debian.org/tracker/firefox-esr</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4549.data"
# $Id: $
