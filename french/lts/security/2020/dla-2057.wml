#use wml::debian::translation-check translation="29d21818df5472d34a746b384877b053ee51078e" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Trois vulnérabilités ont été découvertes dans Pillow, une bibliothèque
d’imagerie pour le langage de programmation Python.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19911">CVE-2019-19911</a>

<p>Il existe une vulnérabilité de déni de service dans Pillow avant 6.2.2 due à
FpxImagePlugin.py appelant la fonction range pour un entier de 32 bits non
validé si le nombre de bandes est grand. Avec Windows exécutant du Python
32 bits, cela aboutit à une OverflowError ou MemoryError due à la limite de
2 GB. Cependant, avec Linux exécutant du Python 64 bits, cela aboutit à
l’arrêt du processus par la fonction OOM killer.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-5312">CVE-2020-5312</a>

<p>La fonction libImaging/PcxDecode.c dans Pillow avant 6.2.2 possède un
dépassement de tampon dans le mode P de PCX.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-5312">CVE-2020-5313</a>

<p>La fonction libImaging/FliDecode.c dans Pillow avant 6.2.2 possède un
dépassement de tampon FLI.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 2.6.1-2+deb8u4.</p>

<p>Nous vous recommandons de mettre à jour vos paquets pillow.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2057.data"
# $Id: $
