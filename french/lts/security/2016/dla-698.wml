#use wml::debian::translation-check translation="ce41d997301872adfc27a79ea546429856226b67" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans qemu, un émulateur
rapide de processeur. Le projet « Common Vulnerabilities and Exposures »
(CVE) identifie les problèmes suivants.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7909">CVE-2016-7909</a>

<p>Quick Emulator (Qemu) construit avec la prise en charge de l'émulateur
PC-Net II d'AMD est vulnérable à un problème de boucle infinie. Il pourrait
survenir lors de la réception de paquets à l'aide de pcnet_receive().</p>

<p>Un utilisateur ou un processus privilégié dans un client pourrait
utiliser ce problème pour planter le processus de Qemu dans l'hôte menant à
un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-8909">CVE-2016-8909</a>

<p>Quick Emulator (Qemu) construit avec la prise en charge de l'émulation
du contrôleur HDA d'Intel est vulnérable à un problème de boucle infinie.
Il pourrait survenir lors du traitement du flux de tampons DMA lors du
transfert de données dans <q>intel_hda_xfer</q>.</p>

<p>Un utilisateur privilégié dans un client pourrait utiliser ce défaut
pour consommer un nombre excessif de cycles de processeur sur l'hôte, avec
pour conséquence un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-8910">CVE-2016-8910</a>

<p>Quick Emulator (Qemu) construit avec la prise en charge de l'émulation
du contrôleur ethernet RTL8139 est vulnérable à un problème de boucle
infinie. Il pourrait survenir lors de la transmission de paquets dans le
mode d'opération C+.</p>

<p>Un utilisateur privilégié dans un client pourrait utiliser ce défaut
pour consommer un nombre excessif de cycles de processeur sur l'hôte, avec
pour conséquence une situation de déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9101">CVE-2016-9101</a>

<p>Quick Emulator (Qemu) construit avec la prise en charge de l'émulation
NIC i8255x (PRO100) est vulnérable à un problème de fuite de mémoire. Il
pourrait survenir lors de la déconnexion du périphérique, et, en le faisant
à de très nombreuses reprises, cela pourrait avoir pour conséquence la
divulgation de la mémoire de l'hôte, affectant d'autres services sur
l'hôte. </p>

<p>Un utilisateur privilégié dans un client pourrait utiliser ce défaut
pour provoquer un déni de service sur l'hôte ou éventuellement le plantage
du processus de Qemu sur l'hôte.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9102">CVE-2016-9102</a> /
<a href="https://security-tracker.debian.org/tracker/CVE-2016-9105">CVE-2016-9105</a> /
<a href="https://security-tracker.debian.org/tracker/CVE-2016-9106">CVE-2016-9106</a>

<p>Quick Emulator (Qemu) construit avec le système de fichiers VirtFS,
partageant un répertoire de l'hôte au moyen de la prise en charge du
système de fichiers de Plan 9 (9pfs), est vulnérable à plusieurs problèmes
de fuite de mémoire.</p>

<p>Un utilisateur privilégié dans un client pourrait utiliser ce défaut
pour divulguer des octets de la mémoire de l'hôte avec pour conséquence un
déni de service pour d'autres services.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9104">CVE-2016-9104</a>

<p>Quick Emulator (Qemu) construit avec le système de fichiers VirtFS,
partageant un répertoire de l'hôte au moyen de la prise en charge du
système de fichiers de Plan 9 (9pfs), est vulnérable à un problème de
dépassement d'entier. Il pourrait survenir en accédant à des valeurs de
xattributes.</p>

<p>Un utilisateur privilégié dans un client pourrait utiliser ce défaut
pour planter l'instance du processus de Qemu avec pour conséquence un déni
de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9103">CVE-2016-9103</a>

<p>Quick Emulator (Qemu) construit avec le système de fichiers VirtFS,
partageant un répertoire de l'hôte au moyen de la prise en charge du
système de fichiers de Plan 9 (9pfs), est vulnérable à un problème de fuite
d'informations. Il pourrait survenir en accédant à une valeur de
xattribute avant qu'elle soit écrite.</p>

<p>Un utilisateur privilégié dans un client pourrait utiliser ce défaut
pour divulguer des octets de la mémoire de l'hôte.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans la
version 1.1.2+dfsg-6+deb7u18.</p>

<p>Nous vous recommandons de mettre à jour vos paquets qemu.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-698.data"
# $Id: $
