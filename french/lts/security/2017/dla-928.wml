#use wml::debian::translation-check translation="adc5cbd36ecf754028e80bbdee567a58bca03b81" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été trouvées dans libsndfile, une bibliothèque
populaire pour lire ou écrire des fichiers audio.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7585">CVE-2017-7585</a>

<p>Dans libsndfile avant 1.0.28, une erreur dans la fonction
« flac_buffer_copy() » (flac.c) peut être exploitée afin de provoquer un
dépassement de pile à l'aide d'un fichier FLAC contrefait spécialement.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7586">CVE-2017-7586</a>

<p>Dans libsndfile avant 1.0.28, une erreur dans la fonction « header_read() »
(common.c) lors du traitement d’étiquettes ID3 peut être exploitée afin de
provoquer un dépassement de pile à l'aide d'un fichier FLAC contrefait
spécialement.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7741">CVE-2017-7741</a>

<p>Dans libsndfile avant 1.0.28, une erreur dans la fonction
« flac_buffer_copy() » (flac.c) peut être exploitée afin de provoquer une
violation de segmentation (avec accès en écriture mémoire) à l'aide d'un fichier
FLAC contrefait spécialement lors d’une tentative de rééchantillonnage, un
problème similaire à
<a href="https://security-tracker.debian.org/tracker/CVE-2017-7585">CVE-2017-7585</a>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7742">CVE-2017-7742</a>

<p>Dans libsndfile avant 1.0.28, une erreur dans la fonction
« flac_buffer_copy() » (flac.c) peut être exploitée afin de provoquer une
violation de segmentation (avec accès en lecture mémoire) à l'aide d'un fichier
FLAC contrefait spécialement lors d’une tentative de rééchantillonnage, un
problème similaire à
<a href="https://security-tracker.debian.org/tracker/CVE-2017-7585">CVE-2017-7585</a>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-9496">CVE-2014-9496</a>

<p>La fonction sd2_parse_rsrc_fork dans sd2.c dans libsndfile permet à des
attaquants d’avoir un impact non précisé à l’aide de vecteurs relatifs à
(1) un décalage de mappage ou (2) Qrsrc marker, qui déclenche une lecture hors
limites.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-9756">CVE-2014-9756</a>

<p>La fonction psf_fwrite dans file_io.c dans libsndfile permet à des attaquants
de provoquer un déni de service (erreur de division par zéro et plantage
d'application) à l’aide de vecteurs non précisés relatifs à la variable
headindex.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-7805">CVE-2015-7805</a>

<p>Un dépassement de tampon basé sur le tas dans libsndfile 1.0.25 permet à des
attaquants distants d’avoir un impact non précisé à l’aide d’une valeur
headindex dans l’en-tête d’un fichier AIFF.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 1.0.25-9.1+deb7u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libsndfile.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-928.data"
# $Id: $
