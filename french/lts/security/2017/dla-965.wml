#use wml::debian::translation-check translation="16a228d71674819599fa1d0027d1603056286470" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans qemu-kvm, une solution de
virtualisation complète pour les hôtes Linux sur du matériel x86 avec des clients
x86, basée sur Quick Emulator (Qemu).</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9602">CVE-2016-9602</a>

<p>Quick Emulator (Qemu) construit avec VirtFS, le partage de répertoires d’hôte
à l’aide de la prise en charge du système de fichiers Plan 9 (9pfs), est vulnérable à
un problème de suivi de lien incorrect. Il pourrait se produire lors de l’accès
à des fichiers de lien symbolique sur un répertoire hôte partagé.</p>

<p>Un utilisateur privilégié dans un client pourrait utiliser ce défaut pour
accéder au système de fichiers hôte au-delà du répertoire partagé et
éventuellement augmenter ses privilèges sur l’hôte.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7377">CVE-2017-7377</a>

<p>Quick Emulator (Qemu) construit avec la prise en charge du dorsal virtio-9p
est vulnérable à un problème de fuite de mémoire. Il pourrait se produire
lors d’une opération d’E/S à l’aide d’une routine v9fs_create/v9fs_lcreate.</p>

<p>Un utilisateur ou processus privilégié dans un client pourrait utiliser ce
défaut pour divulguer la mémoire de l’hôte aboutissant à un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7471">CVE-2017-7471</a>

<p>Quick Emulator (Qemu) construit avec VirtFS, le partage de répertoires d’hôte
à l’aide de la prise en charge du système de fichiers Plan 9 (9pfs), est vulnérable à
un problème de contrôle d’accès incorrect. Il pourrait se produire lors de
l’accès à des fichiers du répertoire partagé de l’hôte.</p>

<p>Un utilisateur privilégié dans un client pourrait utiliser ce défaut pour
accéder au-delà du répertoire partagé et éventuellement augmenter ses privilèges
sur l’hôte.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7493">CVE-2017-7493</a>

<p>Quick Emulator (Qemu) construit avec VirtFS, le partage de répertoires d’hôte
à l’aide de la prise en charge du système de fichiers Plan 9 (9pfs), est vulnérable à un
problème de contrôle d’accès incorrect. Il pourrait se produire lors d’accès
de fichiers de métadonnées virtfs avec le mode de sécurité mapped-file.</p>

<p>Un utilisateur de client pourrait utiliser cela défaut pour augmenter ses
droits dans le client.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-8086">CVE-2017-8086</a>

<p>Quick Emulator (Qemu) construit avec la prise en charge du dorsal virtio-9p
est vulnérable à une fuite de mémoire. Elle pourrait se produire lors de la
requête d’attributs étendus de système de fichiers à l’aide d’une routine
9pfs_list_xattr().</p>

<p>Un utilisateur ou processus privilégié dans un client pourrait utiliser ce
défaut pour divulguer la mémoire de l’hôte aboutissant à un déni de service.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 1.1.2+dfsg-6+deb7u22.</p>

<p>Nous vous recommandons de mettre à jour vos paquets qemu-kvm.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-965.data"
# $Id: $
