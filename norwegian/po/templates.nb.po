msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"PO-Revision-Date: 2016-01-14 09:20+0100\n"
"Last-Translator: Hans Fredrik Nordhaug <hans@nordhaug.priv.no>\n"
"Language-Team: Norwegian Bokmål <i18n-nb@lister.ping.uio.no>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms:  nplurals=2; plural=(n != 1);\n"

#: ../../english/template/debian/basic.wml:19
#: ../../english/template/debian/navbar.wml:11
msgid "Debian"
msgstr "Debian"

#: ../../english/template/debian/basic.wml:46
msgid "Debian website search"
msgstr "Søk i Debians nettsted"

#: ../../english/template/debian/common_translation.wml:4
msgid "Yes"
msgstr "Ja"

#: ../../english/template/debian/common_translation.wml:7
msgid "No"
msgstr "Nei"

#: ../../english/template/debian/common_translation.wml:10
msgid "Debian Project"
msgstr "Debianprosjektet"

#: ../../english/template/debian/common_translation.wml:13
msgid ""
"Debian is an operating system and a distribution of Free Software. It is "
"maintained and updated through the work of many users who volunteer their "
"time and effort."
msgstr ""
"Debian er et operativsystem og en distribusjon av fri programvare. Debain "
"vedlikeholdes og oppdateres av frivillige brukere som donerer tid og arbeid."

#: ../../english/template/debian/common_translation.wml:16
msgid "debian, GNU, linux, unix, open source, free, DFSG"
msgstr "debian, GNU, linux, unix, open source, free, åpen kildekode, fri, DFSG"

#: ../../english/template/debian/common_translation.wml:19
msgid "Back to the <a href=\"m4_HOME/\">Debian Project homepage</a>."
msgstr "Tilbake til <a href=\"m4_HOME/\">Debianprosjektets hjemmeside</a>."

#: ../../english/template/debian/common_translation.wml:22
#: ../../english/template/debian/links.tags.wml:149
msgid "Home"
msgstr "Hjem"

#: ../../english/template/debian/common_translation.wml:25
msgid "Skip Quicknav"
msgstr "Hopp over kjapp navigasjon"

#: ../../english/template/debian/common_translation.wml:28
msgid "About"
msgstr "Om Debian"

#: ../../english/template/debian/common_translation.wml:31
msgid "About Debian"
msgstr "Om Debian"

#: ../../english/template/debian/common_translation.wml:34
msgid "Contact Us"
msgstr "Ta kontakt med oss"

#: ../../english/template/debian/common_translation.wml:37
#, fuzzy
msgid "Legal Info"
msgstr "Informasjon om utgaver"

#: ../../english/template/debian/common_translation.wml:40
msgid "Data Privacy"
msgstr ""

#: ../../english/template/debian/common_translation.wml:43
msgid "Donations"
msgstr "Donasjoner"

#: ../../english/template/debian/common_translation.wml:46
msgid "Events"
msgstr "Begivenheter"

#: ../../english/template/debian/common_translation.wml:49
msgid "News"
msgstr "Nyheter"

#: ../../english/template/debian/common_translation.wml:52
msgid "Distribution"
msgstr "Distribusjon"

#: ../../english/template/debian/common_translation.wml:55
msgid "Support"
msgstr "Brukerstøtte"

#: ../../english/template/debian/common_translation.wml:58
msgid "Pure Blends"
msgstr ""

#: ../../english/template/debian/common_translation.wml:61
#: ../../english/template/debian/links.tags.wml:46
msgid "Developers' Corner"
msgstr "Utviklerhjørnet"

#: ../../english/template/debian/common_translation.wml:64
msgid "Documentation"
msgstr "Dokumentasjon"

#: ../../english/template/debian/common_translation.wml:67
msgid "Security Information"
msgstr "Sikkerhetsinformasjon"

#: ../../english/template/debian/common_translation.wml:70
msgid "Search"
msgstr "Søk"

#: ../../english/template/debian/common_translation.wml:73
msgid "none"
msgstr "ingen"

#: ../../english/template/debian/common_translation.wml:76
msgid "Go"
msgstr "Gå dit"

#: ../../english/template/debian/common_translation.wml:79
msgid "worldwide"
msgstr "verdensomspennende"

#: ../../english/template/debian/common_translation.wml:82
msgid "Site map"
msgstr "Sideoversikt"

#: ../../english/template/debian/common_translation.wml:85
msgid "Miscellaneous"
msgstr "Diverse"

#: ../../english/template/debian/common_translation.wml:88
#: ../../english/template/debian/links.tags.wml:104
msgid "Getting Debian"
msgstr "Få tak i Debian"

#: ../../english/template/debian/common_translation.wml:91
msgid "The Debian Blog"
msgstr "Debians blogg"

#: ../../english/template/debian/common_translation.wml:94
#, fuzzy
msgid "Last Updated"
msgstr "Sist oppdatert"

#: ../../english/template/debian/ddp.wml:6
msgid ""
"Please send all comments, criticisms and suggestions about these web pages "
"to our <a href=\"mailto:debian-doc@lists.debian.org\">mailing list</a>."
msgstr ""
"Send gjerne kommentarer, kritikk og forslag for disse sidene til <a href="
"\"mailto:debian-doc@lists.debian.org\">postlisten</a> vår."

#: ../../english/template/debian/fixes_link.wml:11
msgid "not needed"
msgstr "ikke nødvendig"

#: ../../english/template/debian/fixes_link.wml:14
msgid "not available"
msgstr "Ikke tilgjengelig"

#: ../../english/template/debian/fixes_link.wml:17
msgid "N/A"
msgstr "ikke relevant"

#: ../../english/template/debian/fixes_link.wml:20
msgid "in release 1.1"
msgstr "i utgave 1.1"

#: ../../english/template/debian/fixes_link.wml:23
msgid "in release 1.3"
msgstr "i utgave 1.3"

#: ../../english/template/debian/fixes_link.wml:26
msgid "in release 2.0"
msgstr "i utgave 2.0"

#: ../../english/template/debian/fixes_link.wml:29
msgid "in release 2.1"
msgstr "i utgave 2.1"

#: ../../english/template/debian/fixes_link.wml:32
msgid "in release 2.2"
msgstr "i utgave 2.2"

# Sjekkes!
#. TRANSLATORS: Please make clear in the translation of the following
#. item that mail sent to the debian-www list *must* be in English. Also,
#. you can add some information of your own translation mailing list
#. (i.e. debian-l10n-XXXXXX@lists.debian.org) for reporting things in
#. your language.
#: ../../english/template/debian/footer.wml:106
#, fuzzy
msgid ""
"To report a problem with the web site, please e-mail our publicly archived "
"mailing list <a href=\"mailto:debian-www@lists.debian.org\">debian-www@lists."
"debian.org</a> in English.  For other contact information, see the Debian <a "
"href=\"m4_HOME/contact\">contact page</a>. Web site source code is <a href="
"\"https://salsa.debian.org/webmaster-team/webwml\">available</a>."
msgstr ""
"For å rapportere problemer med nettstedet, send e-post på engelsk til <a "
"href=\"mailto:debian-www@lists.debian.org\">debian-www@lists.debian.org</a>. "
"For øvrig kontaktinformasjon, se Debians <a href=\"m4_HOME/contact"
"\">kontaktside</a>.Nettstedets kildekode er <a href=\"m4_HOME/devel/website/"
"using_cvs\">tilgjengelig</a>."

#: ../../english/template/debian/footer.wml:109
msgid "Last Modified"
msgstr "Sist oppdatert"

#: ../../english/template/debian/footer.wml:112
msgid "Last Built"
msgstr ""

#: ../../english/template/debian/footer.wml:115
msgid "Copyright"
msgstr "Opphavsrett"

#: ../../english/template/debian/footer.wml:118
msgid "<a href=\"https://www.spi-inc.org/\">SPI</a> and others;"
msgstr "<a href=\"https://www.spi-inc.org/\">SPI</a> og andre;"

#: ../../english/template/debian/footer.wml:121
msgid "See <a href=\"m4_HOME/license\" rel=\"copyright\">license terms</a>"
msgstr "Se <a href=\"m4_HOME/license\" rel=\"copyright\">lisensvilkårene</a>"

#: ../../english/template/debian/footer.wml:124
msgid ""
"Debian is a registered <a href=\"m4_HOME/trademark\">trademark</a> of "
"Software in the Public Interest, Inc."
msgstr ""
"Debian er et registrert <a href=\"m4_HOME/trademark\">varemerke</a> hos "
"Software in the Public Interest, Inc."

#: ../../english/template/debian/languages.wml:196
#: ../../english/template/debian/languages.wml:232
msgid "This page is also available in the following languages:"
msgstr "Denne siden er også tilgjengelig på følgende språk:"

#: ../../english/template/debian/languages.wml:265
msgid "How to set <a href=m4_HOME/intro/cn>the default document language</a>"
msgstr "Slik velger du <a href=m4_HOME/intro/cn>standard språk</a>"

#: ../../english/template/debian/languages.wml:323
msgid "Browser default"
msgstr ""

#: ../../english/template/debian/languages.wml:323
msgid "Unset the language override cookie"
msgstr ""

#: ../../english/template/debian/links.tags.wml:4
msgid "Debian International"
msgstr "Debian internasjonal"

#: ../../english/template/debian/links.tags.wml:7
msgid "Partners"
msgstr "Partnere"

#: ../../english/template/debian/links.tags.wml:10
msgid "Debian Weekly News"
msgstr "Debians Ukentlige Nyheter"

#: ../../english/template/debian/links.tags.wml:13
msgid "Weekly News"
msgstr "Ukentlige nyheter"

#: ../../english/template/debian/links.tags.wml:16
msgid "Debian Project News"
msgstr "Debianprosjektnyheter"

#: ../../english/template/debian/links.tags.wml:19
msgid "Project News"
msgstr "Prosjektnyheter"

#: ../../english/template/debian/links.tags.wml:22
msgid "Release Info"
msgstr "Informasjon om utgaver"

#: ../../english/template/debian/links.tags.wml:25
msgid "Debian Packages"
msgstr "Debianpakker"

#: ../../english/template/debian/links.tags.wml:28
msgid "Download"
msgstr "Hent"

#: ../../english/template/debian/links.tags.wml:31
msgid "Debian&nbsp;on&nbsp;CD"
msgstr "Debian&nbsp;på&nbsp;CD-ROM"

#: ../../english/template/debian/links.tags.wml:34
msgid "Debian Books"
msgstr "Debianbøker"

#: ../../english/template/debian/links.tags.wml:37
msgid "Debian Wiki"
msgstr "Debian-wiki"

#: ../../english/template/debian/links.tags.wml:40
msgid "Mailing List Archives"
msgstr "Postlistearkiver"

#: ../../english/template/debian/links.tags.wml:43
msgid "Mailing Lists"
msgstr "Postlister"

#: ../../english/template/debian/links.tags.wml:49
msgid "Social Contract"
msgstr "Sosial kontrakt"

#: ../../english/template/debian/links.tags.wml:52
msgid "Code of Conduct"
msgstr "Etiske regler"

#: ../../english/template/debian/links.tags.wml:55
msgid "Debian 5.0 - The universal operating system"
msgstr "Debian 5.0 - det universelle operativsystemet"

#: ../../english/template/debian/links.tags.wml:58
msgid "Site map for Debian web pages"
msgstr "Oversikt av Debians nettsider"

#: ../../english/template/debian/links.tags.wml:61
msgid "Developer Database"
msgstr "Database av utviklere"

#: ../../english/template/debian/links.tags.wml:64
msgid "Debian FAQ"
msgstr "Debian OSS/FAQ"

#: ../../english/template/debian/links.tags.wml:67
msgid "Debian Policy Manual"
msgstr "Retningslinjehåndbok"

#: ../../english/template/debian/links.tags.wml:70
msgid "Developers' Reference"
msgstr "Utviklerhåndbok"

#: ../../english/template/debian/links.tags.wml:73
msgid "New Maintainers' Guide"
msgstr "Innledning for nye utviklere"

#: ../../english/template/debian/links.tags.wml:76
msgid "Release Critical Bugs"
msgstr "Utgavekritiske feil"

#: ../../english/template/debian/links.tags.wml:79
msgid "Lintian Reports"
msgstr "Lintian-rapporter"

#: ../../english/template/debian/links.tags.wml:83
msgid "Archives for users' mailing lists"
msgstr "Arkiv av brukerpostlister"

#: ../../english/template/debian/links.tags.wml:86
msgid "Archives for developers' mailing lists"
msgstr "Arkiv av utviklerpostlister"

#: ../../english/template/debian/links.tags.wml:89
msgid "Archives for i18n/l10n mailing lists"
msgstr "Arkiv av i18n-/l10n-postlister"

#: ../../english/template/debian/links.tags.wml:92
msgid "Archives for ports' mailing lists"
msgstr "Arkiv av porterings-postlister"

#: ../../english/template/debian/links.tags.wml:95
msgid "Archives for mailing lists of the Bug tracking system"
msgstr "Arkiv av postlister for feilrapportsystemet"

#: ../../english/template/debian/links.tags.wml:98
msgid "Archives for miscellaneous mailing lists"
msgstr "Arkiv av diverse postlister"

#: ../../english/template/debian/links.tags.wml:101
msgid "Free Software"
msgstr "Fri programvare"

#: ../../english/template/debian/links.tags.wml:107
msgid "Development"
msgstr "Utvikling"

#: ../../english/template/debian/links.tags.wml:110
msgid "Help Debian"
msgstr "Hjelp Debian"

#: ../../english/template/debian/links.tags.wml:113
msgid "Bug reports"
msgstr "Feilmeldinger"

#: ../../english/template/debian/links.tags.wml:116
msgid "Ports/Architectures"
msgstr "Arkitekturtilpasninger"

#: ../../english/template/debian/links.tags.wml:119
msgid "Installation manual"
msgstr "Installasjonshåndbok"

#: ../../english/template/debian/links.tags.wml:122
msgid "CD vendors"
msgstr "CD-leverandører"

#: ../../english/template/debian/links.tags.wml:125
msgid "CD/USB ISO images"
msgstr "CD/USB-ISO-avtrykk"

#: ../../english/template/debian/links.tags.wml:128
msgid "Network install"
msgstr "Nettverksinstallasjon"

# Sjekkes!
#: ../../english/template/debian/links.tags.wml:131
msgid "Pre-installed"
msgstr "Ferdiginstallert"

#: ../../english/template/debian/links.tags.wml:134
msgid "Debian-Edu project"
msgstr "Debian-Edu-prosjektet"

#: ../../english/template/debian/links.tags.wml:137
msgid "Alioth &ndash; Debian GForge"
msgstr "Alioth &ndash; Debian GForge"

#: ../../english/template/debian/links.tags.wml:140
msgid "Quality Assurance"
msgstr "Kvalitetskontroll"

#: ../../english/template/debian/links.tags.wml:143
msgid "Package Tracking System"
msgstr "Pakkesporingssystemet"

#: ../../english/template/debian/links.tags.wml:146
msgid "Debian Developer's Packages Overview"
msgstr "Debianutviklernes pakkeoversikt"

#: ../../english/template/debian/navbar.wml:10
msgid "Debian Home"
msgstr "Debian startside"

#: ../../english/template/debian/recent_list.wml:7
msgid "No items for this year."
msgstr "Ingen begivenheter dette året"

#: ../../english/template/debian/recent_list.wml:11
msgid "proposed"
msgstr "foreslått"

#: ../../english/template/debian/recent_list.wml:15
msgid "in discussion"
msgstr "under diskusjon"

#: ../../english/template/debian/recent_list.wml:19
msgid "voting open"
msgstr "avstemning åpen"

#: ../../english/template/debian/recent_list.wml:23
msgid "finished"
msgstr "avsluttet"

#: ../../english/template/debian/recent_list.wml:26
msgid "withdrawn"
msgstr "tilbaketrukket"

#: ../../english/template/debian/recent_list.wml:30
msgid "Future events"
msgstr "Kommende begivenheter"

#: ../../english/template/debian/recent_list.wml:33
msgid "Past events"
msgstr "Tidligere begivenheter"

#: ../../english/template/debian/recent_list.wml:37
msgid "(new revision)"
msgstr "(ny versjon)"

#: ../../english/template/debian/recent_list.wml:303
msgid "Report"
msgstr "Rapport"

#: ../../english/template/debian/redirect.wml:6
msgid "Page redirected to <newpage/>"
msgstr ""

#: ../../english/template/debian/redirect.wml:12
msgid ""
"This page has been renamed to <url <newpage/>>, please update your links."
msgstr ""

#. given a manual name and an architecture, join them
#. if you need to reorder the two, use "%2$s ... %1$s", cf. printf(3)
#: ../../english/template/debian/release.wml:7
msgid "<void id=\"doc_for_arch\" />%s for %s"
msgstr "<void id=\"doc_for_arch\" />%s for %s"

#: ../../english/template/debian/translation-check.wml:37
msgid ""
"<em>Note:</em> The <a href=\"$link\">original document</a> is newer than "
"this translation."
msgstr ""
"<em>NB:</em> <a href=\"$link\">Originalen</a> er nyere enn denne "
"oversettelsen."

#: ../../english/template/debian/translation-check.wml:43
msgid ""
"Warning! This translation is too out of date, please see the <a href=\"$link"
"\">original</a>."
msgstr ""
"Merk! Denne oversettelsen er utdatert, se <a href=\"$link\">originalen</a>."

#: ../../english/template/debian/translation-check.wml:49
msgid ""
"<em>Note:</em> The original document of this translation no longer exists."
msgstr "<em>Merk:</em> Originalen til denne oversettelsen fins ikke lenger."

#: ../../english/template/debian/translation-check.wml:56
msgid "Wrong translation version!"
msgstr ""

#: ../../english/template/debian/url.wml:4
msgid "URL"
msgstr "Nettadresse"

#: ../../english/template/debian/users.wml:12
msgid "Back to the <a href=\"../\">Who's using Debian? page</a>."
msgstr "Tilbake til <a href=\"../\">Hvem bruker Debian? siden</a>."

#: ../../english/search.xml.in:7
msgid "Debian website"
msgstr "Debians nettsted"

#: ../../english/search.xml.in:9
msgid "Search the Debian website."
msgstr "Søk gjennom Debians nettstedet."

#~ msgid "List of Consultants"
#~ msgstr "Konsulentliste"

#~ msgid "Back to the <a href=\"./\">Debian consultants page</a>."
#~ msgstr "Tilbake til <a href=\"./\">Debians konsulentliste</a>."

#~ msgid "&middot;"
#~ msgstr "&middot;"

#, fuzzy
#~ msgid "Download with Jigdo"
#~ msgstr "hent med jigdo"

#, fuzzy
#~ msgid "Download via HTTP/FTP"
#~ msgstr "hent via http/ftp"

#, fuzzy
#~ msgid "Network Install"
#~ msgstr "Nettverksinstallasjon"

#, fuzzy
#~ msgid "<void id=\"dc_download\" />Download"
#~ msgstr "OSS"

#, fuzzy
#~ msgid "<void id=\"dc_misc\" />Misc"
#~ msgstr "diverse"

#, fuzzy
#~ msgid "<void id=\"dc_artwork\" />Artwork"
#~ msgstr "OSS"

#, fuzzy
#~ msgid "<void id=\"dc_mirroring\" />Mirroring"
#~ msgstr "diverse"

#, fuzzy
#~ msgid "<void id=\"dc_rsyncmirrors\" />Rsync Mirrors"
#~ msgstr "diverse"

#~ msgid "Debian CD team"
#~ msgstr "Debians cd-gruppe"

#~ msgid "debian_on_cd"
#~ msgstr "Debian på CD"

#~ msgid "<void id=\"faq-bottom\" />faq"
#~ msgstr "<void id=\"faq-bottom\" />faq"

#~ msgid "jigdo"
#~ msgstr "jigdo"

#~ msgid "http_ftp"
#~ msgstr "http_ftp"

#~ msgid "buy"
#~ msgstr "kjøp"

#~ msgid "net_install"
#~ msgstr "nettinstall"

#~ msgid "<void id=\"misc-bottom\" />misc"
#~ msgstr "diverse"

#~ msgid ""
#~ "English-language <a href=\"/MailingLists/disclaimer\">public mailing "
#~ "list</a> for CDs/DVDs:"
#~ msgstr ""
#~ "Engelskspråklige <a href=\"/MailingLists/disclaimer\">åpne postlister</a> "
#~ "for CD'er/DVD'er:"

#, fuzzy
#~ msgid "Nominations"
#~ msgstr "Donasjoner"

#, fuzzy
#~ msgid "Debate"
#~ msgstr "Debians cd-gruppe"

#, fuzzy
#~ msgid "Proposer"
#~ msgstr "foreslått"

#, fuzzy
#~ msgid "Minimum Discussion"
#~ msgstr "Under&nbsp;diskusjon"

#~ msgid "Waiting&nbsp;for&nbsp;Sponsors"
#~ msgstr "Venter&nbsp;på&nbsp;sponsorer"

#~ msgid "In&nbsp;Discussion"
#~ msgstr "Under&nbsp;diskusjon"

#~ msgid "Voting&nbsp;Open"
#~ msgstr "Avstemming&nbsp;pågår"

#~ msgid "Decided"
#~ msgstr "Besluttet"

#~ msgid "Withdrawn"
#~ msgstr "Tilbaketrukket"

#~ msgid "Other"
#~ msgstr "Annet"

#~ msgid "Home&nbsp;Vote&nbsp;Page"
#~ msgstr "Hjem&nbsp;Avstemmingssiden"

#~ msgid "How&nbsp;To"
#~ msgstr "Hvordan..."

#~ msgid "Submit&nbsp;a&nbsp;Proposal"
#~ msgstr "Send&nbsp;inn&nbsp;et&nbsp;forslag"

#~ msgid "Amend&nbsp;a&nbsp;Proposal"
#~ msgstr "Legg&nbsp;til&nbsp;et&nbsp;forslag"

#~ msgid "Follow&nbsp;a&nbsp;Proposal"
#~ msgstr "Følg&nbsp;et&nbsp;forslag"

#~ msgid "Read&nbsp;a&nbsp;Result"
#~ msgstr "Les&nbsp;et&nbsp;resultat"

#~ msgid "Vote"
#~ msgstr "Stem"

#~ msgid "discussed"
#~ msgstr "diskutert"

#~ msgid "network install"
#~ msgstr "nettverksinstallasjon"

#~ msgid "buy pre-made images"
#~ msgstr "kjøp ferdige cd-avtrykk"

#~ msgid ""
#~ "See the Debian <a href=\"m4_HOME/contact\">contact page</a> for "
#~ "information on contacting us."
#~ msgstr ""
#~ "Se Debians <a href=\"m4_HOME/contact\">kontaktside</a> for informasjon om "
#~ "å komme i kontakt med oss."

#~ msgid "download with pik"
#~ msgstr "hent med pik"

#~ msgid "%0 (dead link)"
#~ msgstr "%0 (død peker)"

#~ msgid "Debian Security Advisory"
#~ msgstr "Sikkerhetsmelding fra Debian"

#~ msgid "Debian Security Advisories"
#~ msgstr "Sikkerhetsmeldinger fra Debian"

#~ msgid "Vulnerable"
#~ msgstr "Sårbar"

#~ msgid "Fixed in"
#~ msgstr "Rettet i"

#~ msgid "Source:"
#~ msgstr "Kildekode:"

#~ msgid "Architecture-independent component:"
#~ msgstr "Arkitekturuavhengig bestanddel:"

#~ msgid ""
#~ "MD5 checksums of the listed files are available in the <a href=\"<get-var "
#~ "url />\">original advisory</a>."
#~ msgstr ""
#~ "MD5-kontrolsummer av disse filene er tilgjengelig i den <a href=\"<get-"
#~ "var url />\">originale sikkerhetsmeldingen</a>."

#~ msgid "No other external database security references currently available."
#~ msgstr ""
#~ "Ingen sikkerhetsreferanser er for tiden tilgjengelig i andre databaser."

#~ msgid "CERT's vulnerabilities, advisories and incident notes:"
#~ msgstr "CERT's notater om sårbarheter, sikkerhet, og observasjoner:"

#~ msgid "In the Bugtraq database (at SecurityFocus):"
#~ msgstr "I Bugtraq-databasen (hos SecurityFocus):"

#~ msgid "In Mitre's CVE dictionary:"
#~ msgstr "I Mitre's CVE-ordbok:"

#~ msgid "Security database references"
#~ msgstr "Referanser i sikkerhetsdatabaser"

#~ msgid "License Information"
#~ msgstr "Lisensinformasjon"

#, fuzzy
#~ msgid "<void id=\"dc_pik\" />Pseudo Image Kit"
#~ msgstr "diverse"

#, fuzzy
#~ msgid ""
#~ "<void id=\"plural\" />This issue of Debian Weekly News was edited by <a "
#~ "href=\"mailto:dwn@debian.org\">%s</a>."
#~ msgstr ""
#~ "Redaktører av Debian Weekly News er <a href=\"mailto:dwn@debian.org\">%s</"
#~ "a>."

#, fuzzy
#~ msgid ""
#~ "<void id=\"singular\" />This issue of Debian Weekly News was edited by <a "
#~ "href=\"mailto:dwn@debian.org\">%s</a>."
#~ msgstr ""
#~ "Redaktør av Debian Weekly News er <a href=\"mailto:dwn@debian.org\">%s</"
#~ "a>."

#~ msgid ""
#~ "<void id=\"plural\" />Debian Weekly News is edited by <a href=\"mailto:"
#~ "dwn@debian.org\">%s</a>."
#~ msgstr ""
#~ "Redaktører av Debian Weekly News er <a href=\"mailto:dwn@debian.org\">%s</"
#~ "a>."

#~ msgid ""
#~ "<void id=\"singular\" />Debian Weekly News is edited by <a href=\"mailto:"
#~ "dwn@debian.org\">%s</a>."
#~ msgstr ""
#~ "Redaktør av Debian Weekly News er <a href=\"mailto:dwn@debian.org\">%s</"
#~ "a>."

#~ msgid ""
#~ "To receive this newsletter weekly in your mailbox, <a href=\"http://lists."
#~ "debian.org/debian-news/\">subscribe to the debian-news mailing list</a>."
#~ msgstr ""
#~ "For å få dette nyhetsbrevet per e-post hver uke, <a href=\"http://lists."
#~ "debian.org/debian-news/\">abonner</a> på postlisten debian-news."

#, fuzzy
#~ msgid "<void id=\"pluralfemale\" />It was translated by %s."
#~ msgstr ""
#~ "Redaktører av Debian Weekly News er <a href=\"mailto:dwn@debian.org\">%s</"
#~ "a>."

#, fuzzy
#~ msgid "<void id=\"singularfemale\" />It was translated by %s."
#~ msgstr ""
#~ "Redaktør av Debian Weekly News er <a href=\"mailto:dwn@debian.org\">%s</"
#~ "a>."

#, fuzzy
#~ msgid "<void id=\"plural\" />It was translated by %s."
#~ msgstr ""
#~ "Redaktører av Debian Weekly News er <a href=\"mailto:dwn@debian.org\">%s</"
#~ "a>."

#, fuzzy
#~ msgid "<void id=\"singular\" />It was translated by %s."
#~ msgstr ""
#~ "Redaktør av Debian Weekly News er <a href=\"mailto:dwn@debian.org\">%s</"
#~ "a>."

#, fuzzy
#~ msgid ""
#~ "<void id=\"plural\" />This issue of Debian Project News was edited by <a "
#~ "href=\"mailto:debian-publicity@lists.debian.org\">%s</a>."
#~ msgstr ""
#~ "Redaktører av Debian Weekly News er <a href=\"mailto:dwn@debian.org\">%s</"
#~ "a>."

#, fuzzy
#~ msgid ""
#~ "<void id=\"singular\" />This issue of Debian Project News was edited by "
#~ "<a href=\"mailto:debian-publicity@lists.debian.org\">%s</a>."
#~ msgstr ""
#~ "Redaktør av Debian Weekly News er <a href=\"mailto:dwn@debian.org\">%s</"
#~ "a>."

#, fuzzy
#~| msgid ""
#~| "<void id=\"plural\" />Debian Weekly News is edited by <a href=\"mailto:"
#~| "dwn@debian.org\">%s</a>."
#~ msgid ""
#~ "<void id=\"plural\" />Debian Project News is edited by <a href=\"mailto:"
#~ "debian-publicity@lists.debian.org\">%s</a>."
#~ msgstr ""
#~ "Redaktører av Debian Weekly News er <a href=\"mailto:dwn@debian.org\">%s</"
#~ "a>."

#, fuzzy
#~| msgid ""
#~| "<void id=\"singular\" />Debian Weekly News is edited by <a href=\"mailto:"
#~| "dwn@debian.org\">%s</a>."
#~ msgid ""
#~ "<void id=\"singular\" />Debian Project News is edited by <a href=\"mailto:"
#~ "debian-publicity@lists.debian.org\">%s</a>."
#~ msgstr ""
#~ "Redaktør av Debian Weekly News er <a href=\"mailto:dwn@debian.org\">%s</"
#~ "a>."

#~ msgid "<a href=\"../../\">Back issues</a> of this newsletter are available."
#~ msgstr ""
#~ "<a href=\"../../\">Tidligere utgaver</a> av nyhetsbrevet er tilgjengelige."

#, fuzzy
#~| msgid ""
#~| "To receive this newsletter weekly in your mailbox, <a href=\"http://"
#~| "lists.debian.org/debian-news/\">subscribe to the debian-news mailing "
#~| "list</a>."
#~ msgid ""
#~ "To receive this newsletter bi-weekly in your mailbox, <a href=\"http://"
#~ "lists.debian.org/debian-news/\">subscribe to the debian-news mailing "
#~ "list</a>."
#~ msgstr ""
#~ "For å få dette nyhetsbrevet per e-post hver uke, <a href=\"http://lists."
#~ "debian.org/debian-news/\">abonner</a> på postlisten debian-news."

#~ msgid "<get-var url /> (dead link)"
#~ msgstr "<get-var url /> (død peker)"

# Sjekkes!
#~ msgid ""
#~ "Back to: other <a href=\"./\">Debian news</a> || <a href=\"m4_HOME/"
#~ "\">Debian Project homepage</a>."
#~ msgstr ""
#~ "Tilbake til: andre <a href=\"./\">Debiannyheter</a> || <a href=\"m4_HOME/"
#~ "\">Debianprosjektets hjemmeside</a>."

#~ msgid "Latest News"
#~ msgstr "Siste nytt"

#~ msgid "Related Links"
#~ msgstr "Beslektede pekere"

#~ msgid "<th>Project</th><th>Coordinator</th>"
#~ msgstr "<th>Prosjekt</th><th>Koordinator</th>"

#~ msgid "Main Coordinator"
#~ msgstr "Hovedkoordinator"

#~ msgid "Debian Involvement"
#~ msgstr "Debians engasjement"

#~ msgid "More Info"
#~ msgstr "Mer informasjon"

#~ msgid "Where"
#~ msgstr "Hvor"

#~ msgid "When"
#~ msgstr "Når"

#~ msgid "link may no longer be valid"
#~ msgstr "pekeren er muligvis gammel"

#~ msgid "Upcoming Attractions"
#~ msgstr "Kommende begivenheter"

#~ msgid "More information"
#~ msgstr "Mer informasjon"

#~ msgid "Rating:"
#~ msgstr "Vurdering:"

#~ msgid "Nobody"
#~ msgstr "Ingen"

#~ msgid "Taken by:"
#~ msgstr "Tatt av:"

#~ msgid "More information:"
#~ msgstr "Mer informasjon:"

#~| msgid "Select a server near you"
#~ msgid "Select a server near you: &nbsp;"
#~ msgstr "Velg en tjener nær deg: &nbsp;"

#~| msgid "Report"
#~ msgid "Report it!"
#~ msgstr "Rapporter det"

#~ msgid "Have you found a problem with the site layout?"
#~ msgstr "Har du oppdaget et problem med nettstedets utforming?"

#~ msgid "Visit the site sponsor"
#~ msgstr "Besøk sponsoren for disse sidene"
