# Translation of coutries.pot to Slovak
# Copyright (C)
# This file is distributed under the same license as the webwml package.
# Ivan Masár <helix84@centrum.sk>, 2009, 2011, 2017.
msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"Report-Msgid-Bugs-To: debian-www@lists.debian.org\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2017-07-12 16:39+0200\n"
"Last-Translator: Ivan Masár <helix84@centrum.sk>\n"
"Language-Team: x\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"
"X-Generator: Virtaal 0.7.1\n"

#: ../../english/template/debian/countries.wml:111
msgid "United Arab Emirates"
msgstr "Spojené arabské emiráty"

#: ../../english/template/debian/countries.wml:114
msgid "Albania"
msgstr "Albánsko"

#: ../../english/template/debian/countries.wml:117
msgid "Armenia"
msgstr "Arménsko"

#: ../../english/template/debian/countries.wml:120
msgid "Argentina"
msgstr "Argentína"

#: ../../english/template/debian/countries.wml:123
msgid "Austria"
msgstr "Rakúsko"

#: ../../english/template/debian/countries.wml:126
msgid "Australia"
msgstr "Austrália"

#: ../../english/template/debian/countries.wml:129
msgid "Bosnia and Herzegovina"
msgstr "Bosna a Hercegovina"

#: ../../english/template/debian/countries.wml:132
msgid "Bangladesh"
msgstr "Bangladéš"

#: ../../english/template/debian/countries.wml:135
msgid "Belgium"
msgstr "Belgicko"

#: ../../english/template/debian/countries.wml:138
msgid "Bulgaria"
msgstr "Bulharsko"

#: ../../english/template/debian/countries.wml:141
msgid "Brazil"
msgstr "Brazília"

#: ../../english/template/debian/countries.wml:144
msgid "Bahamas"
msgstr "Bahamy"

#: ../../english/template/debian/countries.wml:147
msgid "Belarus"
msgstr "Bielorusko"

#: ../../english/template/debian/countries.wml:150
msgid "Canada"
msgstr "Kanada"

#: ../../english/template/debian/countries.wml:153
msgid "Switzerland"
msgstr "Švajčiarsko"

#: ../../english/template/debian/countries.wml:156
msgid "Chile"
msgstr "Čile"

#: ../../english/template/debian/countries.wml:159
msgid "China"
msgstr "Čína"

#: ../../english/template/debian/countries.wml:162
msgid "Colombia"
msgstr "Kolumbia"

#: ../../english/template/debian/countries.wml:165
msgid "Costa Rica"
msgstr "Kostarika"

#: ../../english/template/debian/countries.wml:168
msgid "Czech Republic"
msgstr "Česká republika"

#: ../../english/template/debian/countries.wml:171
msgid "Germany"
msgstr "Nemecko"

#: ../../english/template/debian/countries.wml:174
msgid "Denmark"
msgstr "Dánsko"

#: ../../english/template/debian/countries.wml:177
msgid "Dominican Republic"
msgstr "Dominikánska republika"

#: ../../english/template/debian/countries.wml:180
msgid "Algeria"
msgstr "Alžírsko"

#: ../../english/template/debian/countries.wml:183
msgid "Ecuador"
msgstr "Ekvádor"

#: ../../english/template/debian/countries.wml:186
msgid "Estonia"
msgstr "Estónsko"

#: ../../english/template/debian/countries.wml:189
msgid "Egypt"
msgstr "Egypt"

#: ../../english/template/debian/countries.wml:192
msgid "Spain"
msgstr "Španielsko"

#: ../../english/template/debian/countries.wml:195
msgid "Ethiopia"
msgstr "Etiópia"

#: ../../english/template/debian/countries.wml:198
msgid "Finland"
msgstr "Fínsko"

#: ../../english/template/debian/countries.wml:201
msgid "Faroe Islands"
msgstr "Faerské ostrovy"

#: ../../english/template/debian/countries.wml:204
msgid "France"
msgstr "Francúzsko"

#: ../../english/template/debian/countries.wml:207
msgid "United Kingdom"
msgstr "Spojené kráľovstvo"

#: ../../english/template/debian/countries.wml:210
msgid "Grenada"
msgstr "Grenada"

#: ../../english/template/debian/countries.wml:213
msgid "Georgia"
msgstr "Gruzínsko"

#: ../../english/template/debian/countries.wml:216
msgid "Greenland"
msgstr "Grónsko"

#: ../../english/template/debian/countries.wml:219
msgid "Greece"
msgstr "Grécko"

#: ../../english/template/debian/countries.wml:222
msgid "Guatemala"
msgstr "Guatemala"

#: ../../english/template/debian/countries.wml:225
msgid "Hong Kong"
msgstr "Hongkong"

#: ../../english/template/debian/countries.wml:228
msgid "Honduras"
msgstr "Honduras"

#: ../../english/template/debian/countries.wml:231
msgid "Croatia"
msgstr "Chorvátsko"

#: ../../english/template/debian/countries.wml:234
msgid "Hungary"
msgstr "Maďarsko"

#: ../../english/template/debian/countries.wml:237
msgid "Indonesia"
msgstr "Indonézia"

#: ../../english/template/debian/countries.wml:240
msgid "Iran"
msgstr "Irán"

#: ../../english/template/debian/countries.wml:243
msgid "Ireland"
msgstr "Írsko"

#: ../../english/template/debian/countries.wml:246
msgid "Israel"
msgstr "Izrael"

#: ../../english/template/debian/countries.wml:249
msgid "India"
msgstr "India"

#: ../../english/template/debian/countries.wml:252
msgid "Iceland"
msgstr "Island"

#: ../../english/template/debian/countries.wml:255
msgid "Italy"
msgstr "Taliansko"

#: ../../english/template/debian/countries.wml:258
msgid "Jordan"
msgstr "Jordánsko"

#: ../../english/template/debian/countries.wml:261
msgid "Japan"
msgstr "Japonsko"

#: ../../english/template/debian/countries.wml:264
msgid "Kenya"
msgstr "Keňa"

#: ../../english/template/debian/countries.wml:267
msgid "Kyrgyzstan"
msgstr "Kirgizsko"

#: ../../english/template/debian/countries.wml:270
msgid "Korea"
msgstr "Kórea"

#: ../../english/template/debian/countries.wml:273
msgid "Kuwait"
msgstr "Kuvajt"

#: ../../english/template/debian/countries.wml:276
msgid "Kazakhstan"
msgstr "Kazachstan"

#: ../../english/template/debian/countries.wml:279
msgid "Sri Lanka"
msgstr "Srí Lanka"

#: ../../english/template/debian/countries.wml:282
msgid "Lithuania"
msgstr "Litva"

#: ../../english/template/debian/countries.wml:285
msgid "Luxembourg"
msgstr "Luxembursko"

#: ../../english/template/debian/countries.wml:288
msgid "Latvia"
msgstr "Lotyšsko"

#: ../../english/template/debian/countries.wml:291
msgid "Morocco"
msgstr "Maroko"

#: ../../english/template/debian/countries.wml:294
msgid "Moldova"
msgstr "Moldavsko"

#: ../../english/template/debian/countries.wml:297
msgid "Montenegro"
msgstr "Čierna Hora"

#: ../../english/template/debian/countries.wml:300
msgid "Madagascar"
msgstr "Madagaskar"

#: ../../english/template/debian/countries.wml:303
msgid "Macedonia, Republic of"
msgstr "Macedónska republika"

#: ../../english/template/debian/countries.wml:306
msgid "Mongolia"
msgstr "Mongolsko"

#: ../../english/template/debian/countries.wml:309
msgid "Malta"
msgstr "Malta"

#: ../../english/template/debian/countries.wml:312
msgid "Mexico"
msgstr "Mexiko"

#: ../../english/template/debian/countries.wml:315
msgid "Malaysia"
msgstr "Malajzia"

#: ../../english/template/debian/countries.wml:318
msgid "New Caledonia"
msgstr "Nová Kaledónia"

#: ../../english/template/debian/countries.wml:321
msgid "Nicaragua"
msgstr "Nikaragua"

#: ../../english/template/debian/countries.wml:324
msgid "Netherlands"
msgstr "Holandsko"

#: ../../english/template/debian/countries.wml:327
msgid "Norway"
msgstr "Nórsko"

#: ../../english/template/debian/countries.wml:330
msgid "New Zealand"
msgstr "Nový Zéland"

#: ../../english/template/debian/countries.wml:333
msgid "Panama"
msgstr "Panama"

#: ../../english/template/debian/countries.wml:336
msgid "Peru"
msgstr "Peru"

#: ../../english/template/debian/countries.wml:339
msgid "French Polynesia"
msgstr "Francúzska Polynézia"

#: ../../english/template/debian/countries.wml:342
msgid "Philippines"
msgstr "Filipíny"

#: ../../english/template/debian/countries.wml:345
msgid "Pakistan"
msgstr "Pakistan"

#: ../../english/template/debian/countries.wml:348
msgid "Poland"
msgstr "Poľsko"

#: ../../english/template/debian/countries.wml:351
msgid "Portugal"
msgstr "Portugalsko"

#: ../../english/template/debian/countries.wml:354
msgid "Réunion"
msgstr "Réunion"

#: ../../english/template/debian/countries.wml:357
msgid "Romania"
msgstr "Rumunsko"

#: ../../english/template/debian/countries.wml:360
msgid "Serbia"
msgstr "Srbsko"

#: ../../english/template/debian/countries.wml:363
msgid "Russia"
msgstr "Rusko"

#: ../../english/template/debian/countries.wml:366
msgid "Saudi Arabia"
msgstr "Saudská Arábia"

#: ../../english/template/debian/countries.wml:369
msgid "Sweden"
msgstr "Švédsko"

#: ../../english/template/debian/countries.wml:372
msgid "Singapore"
msgstr "Singapur"

#: ../../english/template/debian/countries.wml:375
msgid "Slovenia"
msgstr "Slovinsko"

#: ../../english/template/debian/countries.wml:378
msgid "Slovakia"
msgstr "Slovensko"

#: ../../english/template/debian/countries.wml:381
msgid "El Salvador"
msgstr "Salvádor"

#: ../../english/template/debian/countries.wml:384
msgid "Thailand"
msgstr "Thajsko"

#: ../../english/template/debian/countries.wml:387
msgid "Tajikistan"
msgstr "Tadžikistan"

#: ../../english/template/debian/countries.wml:390
msgid "Tunisia"
msgstr "Tunisko"

#: ../../english/template/debian/countries.wml:393
msgid "Turkey"
msgstr "Turecko"

#: ../../english/template/debian/countries.wml:396
msgid "Taiwan"
msgstr "Taiwan"

#: ../../english/template/debian/countries.wml:399
msgid "Ukraine"
msgstr "Ukrajina"

#: ../../english/template/debian/countries.wml:402
msgid "United States"
msgstr "Spojené štáty"

#: ../../english/template/debian/countries.wml:405
msgid "Uruguay"
msgstr "Uruguaj"

#: ../../english/template/debian/countries.wml:408
msgid "Uzbekistan"
msgstr "Uzbekistan"

#: ../../english/template/debian/countries.wml:411
msgid "Venezuela"
msgstr "Venezuela"

#: ../../english/template/debian/countries.wml:414
msgid "Vietnam"
msgstr "Vietnam"

#: ../../english/template/debian/countries.wml:417
msgid "Vanuatu"
msgstr "Vanuatu"

#: ../../english/template/debian/countries.wml:420
msgid "South Africa"
msgstr "Južná Afrika"

#: ../../english/template/debian/countries.wml:423
msgid "Zimbabwe"
msgstr "Zimbabwe"

#~ msgid "Afghanistan"
#~ msgstr "Afganistan"

#, fuzzy
#~ msgid "Islamic Republic of Afghanistan"
#~ msgstr "Pakistanská islamská republika"

#~ msgid "Åland Islands"
#~ msgstr "Ålandy"

#~ msgid "Republic of Albania"
#~ msgstr "Albánska republika"

#~ msgid "People's Democratic Republic of Algeria"
#~ msgstr "Alžírska demokratická ľudová republika"

#~ msgid "American Samoa"
#~ msgstr "Americká Samoa"

#~ msgid "Andorra"
#~ msgstr "Andorra"

#~ msgid "Principality of Andorra"
#~ msgstr "Andorrské kniežatstvo"

#~ msgid "Angola"
#~ msgstr "Angola"

#~ msgid "Republic of Angola"
#~ msgstr "Angolská republika"

#~ msgid "Anguilla"
#~ msgstr "Anguilla"

#~ msgid "Antarctica"
#~ msgstr "Antarktída"

#~ msgid "Antigua and Barbuda"
#~ msgstr "Antigua a Barbuda"

#~ msgid "Argentine Republic"
#~ msgstr "Argentínska republika"

#~ msgid "Republic of Armenia"
#~ msgstr "Arménska republika"

#~ msgid "Aruba"
#~ msgstr "Aruba"

#~ msgid "Republic of Austria"
#~ msgstr "Rakúska republika"

#~ msgid "Azerbaijan"
#~ msgstr "Azerbajdžan"

#~ msgid "Republic of Azerbaijan"
#~ msgstr "Azerbajdžanská republika"

#~ msgid "Commonwealth of the Bahamas"
#~ msgstr "Bahamské spoločenstvo"

#~ msgid "Bahrain"
#~ msgstr "Bahrajn"

#, fuzzy
#~ msgid "Kingdom of Bahrain"
#~ msgstr "Bhutánske kráľovstvo"

#~ msgid "People's Republic of Bangladesh"
#~ msgstr "Bangladéšska ľudová republika"

#~ msgid "Barbados"
#~ msgstr "Barbados"

#~ msgid "Republic of Belarus"
#~ msgstr "Bieloruská republika"

#~ msgid "Kingdom of Belgium"
#~ msgstr "Belgické kráľovstvo"

#~ msgid "Belize"
#~ msgstr "Belize"

#~ msgid "Benin"
#~ msgstr "Benin"

#~ msgid "Republic of Benin"
#~ msgstr "Beninská republika"

#~ msgid "Bermuda"
#~ msgstr "Bermudy"

#~ msgid "Bhutan"
#~ msgstr "Bhután"

#~ msgid "Kingdom of Bhutan"
#~ msgstr "Bhutánske kráľovstvo"

#~ msgid "Bolivia"
#~ msgstr "Bolívia"

#~ msgid "Republic of Bolivia"
#~ msgstr "Bolívijská republika"

#~ msgid "Republic of Bosnia and Herzegovina"
#~ msgstr "Republika Bosny a Hercegoviny"

#~ msgid "Botswana"
#~ msgstr "Botswana"

#~ msgid "Republic of Botswana"
#~ msgstr "Botswanská republika"

#~ msgid "Bouvet Island"
#~ msgstr "Bouvetov ostrov"

#~ msgid "Federative Republic of Brazil"
#~ msgstr "Brazílska federatívna republika"

#~ msgid "British Indian Ocean Territory"
#~ msgstr "Britské indickooceánske územie"

#~ msgid "Brunei Darussalam"
#~ msgstr "Brunejsko-darussalamský štát"

#~ msgid "Republic of Bulgaria"
#~ msgstr "Bulharská republika"

#~ msgid "Burkina Faso"
#~ msgstr "Burkina Faso"

#~ msgid "Burundi"
#~ msgstr "Burundi"

#~ msgid "Republic of Burundi"
#~ msgstr "Burundská republika"

#~ msgid "Cambodia"
#~ msgstr "Kambodža"

#~ msgid "Kingdom of Cambodia"
#~ msgstr "Kambodžské kráľovstvo"

#~ msgid "Cameroon"
#~ msgstr "Kamerun"

#~ msgid "Republic of Cameroon"
#~ msgstr "Kamerunská republika"

#~ msgid "Cape Verde"
#~ msgstr "Kapverdy"

#~ msgid "Republic of Cape Verde"
#~ msgstr "Kapverdská republika"

#~ msgid "Cayman Islands"
#~ msgstr "Kajmanie ostrovy"

#~ msgid "Central African Republic"
#~ msgstr "Stredoafrická republika"

#~ msgid "Chad"
#~ msgstr "Čad"

#~ msgid "Republic of Chad"
#~ msgstr "Čadská republika"

#~ msgid "Republic of Chile"
#~ msgstr "Čilská republika"

#~ msgid "People's Republic of China"
#~ msgstr "Čínska ľudová republika"

#~ msgid "Christmas Island"
#~ msgstr "Vianočný ostrov"

#~ msgid "Cocos (Keeling) Islands"
#~ msgstr "Kokosové ostrovy"

#~ msgid "Republic of Colombia"
#~ msgstr "Kolumbijská republika"

#~ msgid "Comoros"
#~ msgstr "Komory"

#~ msgid "Union of the Comoros"
#~ msgstr "Komorský zväz"

#~ msgid "Congo"
#~ msgstr "Kongo"

#~ msgid "Republic of the Congo"
#~ msgstr "Konžská republika"

#~ msgid "Congo, The Democratic Republic of the"
#~ msgstr "Konžská demokratická republika"

#~ msgid "Cook Islands"
#~ msgstr "Cookove ostrovy"

#~ msgid "Republic of Costa Rica"
#~ msgstr "Kostarická republika"

#~ msgid "Côte d'Ivoire"
#~ msgstr "Pobrežie Slonoviny"

#, fuzzy
#~ msgid "Republic of Côte d'Ivoire"
#~ msgstr "Republika Pobrežia Slonoviny"

#~ msgid "Republic of Croatia"
#~ msgstr "Chorvátska republika"

#~ msgid "Cuba"
#~ msgstr "Kuba"

#~ msgid "Republic of Cuba"
#~ msgstr "Kubánska republika"

#~ msgid "Cyprus"
#~ msgstr "Cyprus"

#~ msgid "Republic of Cyprus"
#~ msgstr "Cyperská republika"

#~ msgid "Kingdom of Denmark"
#~ msgstr "Dánske kráľovstvo"

#~ msgid "Djibouti"
#~ msgstr "Džibutsko"

#~ msgid "Republic of Djibouti"
#~ msgstr "Džibutská republika"

#~ msgid "Dominica"
#~ msgstr "Dominika"

#~ msgid "Commonwealth of Dominica"
#~ msgstr "Dominické spoločenstvo"

#~ msgid "Republic of Ecuador"
#~ msgstr "Ekvádorská republika"

#~ msgid "Arab Republic of Egypt"
#~ msgstr "Egyptská arabská republika"

#~ msgid "Republic of El Salvador"
#~ msgstr "Salvádorská republika"

#~ msgid "Equatorial Guinea"
#~ msgstr "Rovníková Guinea"

#~ msgid "Republic of Equatorial Guinea"
#~ msgstr "Republika Rovníkovej Guiney"

#~ msgid "Eritrea"
#~ msgstr "Eritrea"

#~ msgid "Republic of Estonia"
#~ msgstr "Estónska republika"

#~ msgid "Federal Democratic Republic of Ethiopia"
#~ msgstr "Etiópska federatívna demokratická republika"

#~ msgid "Falkland Islands (Malvinas)"
#~ msgstr "Falklandy (Malvíny)"

#~ msgid "Fiji"
#~ msgstr "Fidži"

#~ msgid "Republic of the Fiji Islands"
#~ msgstr "Republika Fidžijských ostrovov"

#~ msgid "Republic of Finland"
#~ msgstr "Fínska republika"

#~ msgid "French Republic"
#~ msgstr "Francúzska republika"

#~ msgid "French Guiana"
#~ msgstr "Francúzska Guyana"

#~ msgid "French Southern Territories"
#~ msgstr "Francúzske južné a antarktické územia"

#~ msgid "Gabon"
#~ msgstr "Gabon"

#~ msgid "Gabonese Republic"
#~ msgstr "Gabonská republika"

#~ msgid "Gambia"
#~ msgstr "Gambia"

#~ msgid "Republic of the Gambia"
#~ msgstr "Gambijská republika"

#~ msgid "Federal Republic of Germany"
#~ msgstr "Nemecká spolková republika"

#~ msgid "Ghana"
#~ msgstr "Ghana"

#~ msgid "Republic of Ghana"
#~ msgstr "Ghanská republika"

#~ msgid "Gibraltar"
#~ msgstr "Gibraltár"

# pozn.: Na výslovnú žiadosť gréckej strany sa v medzinárodných dohodách, zmluvách a podobných dokumentoch medzi Gréckou republikou a Slovenskou republikou používa podoba Helénska republika, v ostatných prípadoch podoba Grécka republika.
#~ msgid "Hellenic Republic"
#~ msgstr "Grécka republika"

#~ msgid "Guadeloupe"
#~ msgstr "Guadeloupe"

#~ msgid "Guam"
#~ msgstr "Guam"

#~ msgid "Republic of Guatemala"
#~ msgstr "Guatemalská republika"

#~ msgid "Guernsey"
#~ msgstr "Guernsey"

#~ msgid "Guinea"
#~ msgstr "Guinea"

#~ msgid "Republic of Guinea"
#~ msgstr "Guinejská republika"

#~ msgid "Guinea-Bissau"
#~ msgstr "Guinea-Bissau"

#~ msgid "Republic of Guinea-Bissau"
#~ msgstr "Guinejsko-bissauská republika"

#~ msgid "Guyana"
#~ msgstr "Guyana"

#~ msgid "Republic of Guyana"
#~ msgstr "Guyanská kooperatívna republika"

#~ msgid "Haiti"
#~ msgstr "Haiti"

#~ msgid "Republic of Haiti"
#~ msgstr "Haitská republika"

#~ msgid "Heard Island and McDonald Islands"
#~ msgstr "Heardov ostrov"

#~ msgid "Holy See (Vatican City State)"
#~ msgstr "Svätá stolica (Vatikánsky mestský štát)"

#~ msgid "Republic of Honduras"
#~ msgstr "Honduraská republika"

#~ msgid "Hong Kong Special Administrative Region of China"
#~ msgstr "Osobitná administratívna oblasť Číny Hongkong"

#~ msgid "Republic of Hungary"
#~ msgstr "Maďarská republika"

#~ msgid "Republic of Iceland"
#~ msgstr "Islandská republika"

#~ msgid "Republic of India"
#~ msgstr "Indická republika"

#~ msgid "Republic of Indonesia"
#~ msgstr "Indonézska republika"

#~ msgid "Iran, Islamic Republic of"
#~ msgstr "Iránska islamská republika"

#~ msgid "Islamic Republic of Iran"
#~ msgstr "Iránska islamská republika"

#~ msgid "Republic of Iraq"
#~ msgstr "Iracká republika"

#~ msgid "Isle of Man"
#~ msgstr "Man"

#~ msgid "State of Israel"
#~ msgstr "Izraelský štát"

#~ msgid "Italian Republic"
#~ msgstr "Talianska republika"

#~ msgid "Jamaica"
#~ msgstr "Jamajka"

#~ msgid "Jersey"
#~ msgstr "Jersey"

#~ msgid "Hashemite Kingdom of Jordan"
#~ msgstr "Jordánske hášimovské kráľovstvo"

#~ msgid "Republic of Kazakhstan"
#~ msgstr "Kazašská republika"

#~ msgid "Republic of Kenya"
#~ msgstr "Kenská republika"

#~ msgid "Kiribati"
#~ msgstr "Kiribati"

#~ msgid "Republic of Kiribati"
#~ msgstr "Kiribatská republika"

#~ msgid "Korea, Democratic People's Republic of"
#~ msgstr "Kórejská ľudovodemokratická republika"

#~ msgid "Democratic People's Republic of Korea"
#~ msgstr "Kórejská ľudovodemokratická republika"

#~ msgid "Korea, Republic of"
#~ msgstr "Kórejská republika"

#~ msgid "State of Kuwait"
#~ msgstr "Kuvajtský štát"

#~ msgid "Kyrgyz Republic"
#~ msgstr "Kirgizská republika"

#~ msgid "Lao People's Democratic Republic"
#~ msgstr "Laoská ľudovodemokratická republika"

#~ msgid "Republic of Latvia"
#~ msgstr "Lotyšská republika"

#~ msgid "Lebanon"
#~ msgstr "Libanon"

#~ msgid "Lebanese Republic"
#~ msgstr "Libanonská republika"

#~ msgid "Lesotho"
#~ msgstr "Lesotho"

#~ msgid "Kingdom of Lesotho"
#~ msgstr "Lesothské kráľovstvo"

#~ msgid "Liberia"
#~ msgstr "Libéria"

#~ msgid "Republic of Liberia"
#~ msgstr "Libérijská republika"

#~ msgid "Libyan Arab Jamahiriya"
#~ msgstr "Líbya"

#~ msgid "Socialist People's Libyan Arab Jamahiriya"
#~ msgstr "Líbyjská arabská ľudová socialistická džamahírija"

#~ msgid "Liechtenstein"
#~ msgstr "Lichtenštajnsko"

#~ msgid "Principality of Liechtenstein"
#~ msgstr "Lichtenštajnské kniežatstvo"

#~ msgid "Republic of Lithuania"
#~ msgstr "Litovská republika"

#~ msgid "Grand Duchy of Luxembourg"
#~ msgstr "Luxemburské veľkovojvodstvo"

#~ msgid "Macao"
#~ msgstr "Macao"

#~ msgid "Macao Special Administrative Region of China"
#~ msgstr "Osobitná administratívna oblasť Číny Macao"

#~ msgid "The Former Yugoslav Republic of Macedonia"
#~ msgstr "Bývalá juhoslovanská republika Macedónsko"

#~ msgid "Republic of Madagascar"
#~ msgstr "Madagaskarská republika"

#~ msgid "Malawi"
#~ msgstr "Malawi"

#~ msgid "Republic of Malawi"
#~ msgstr "Malawijská republika"

#~ msgid "Maldives"
#~ msgstr "Maldivy"

#~ msgid "Republic of Maldives"
#~ msgstr "Maldivská republika"

#~ msgid "Mali"
#~ msgstr "Mali"

#~ msgid "Republic of Mali"
#~ msgstr "Malijská republika"

#~ msgid "Republic of Malta"
#~ msgstr "Maltská republika"

#~ msgid "Marshall Islands"
#~ msgstr "Marshallove ostrovy"

#~ msgid "Republic of the Marshall Islands"
#~ msgstr "Republika Marshallových ostrovov"

#~ msgid "Martinique"
#~ msgstr "Martinik"

#~ msgid "Mauritania"
#~ msgstr "Mauritánia"

#~ msgid "Islamic Republic of Mauritania"
#~ msgstr "Mauritánska islamská republika"

#~ msgid "Mauritius"
#~ msgstr "Maurícius"

#~ msgid "Republic of Mauritius"
#~ msgstr "Maurícijská republika"

#~ msgid "Mayotte"
#~ msgstr "Mayotte"

#~ msgid "United Mexican States"
#~ msgstr "Spojené štáty mexické"

#~ msgid "Micronesia, Federated States of"
#~ msgstr "Mikronézske federatívne štáty"

#~ msgid "Federated States of Micronesia"
#~ msgstr "Mikronézske federatívne štáty"

#~ msgid "Republic of Moldova"
#~ msgstr "Moldavská republika"

#~ msgid "Monaco"
#~ msgstr "Monako"

#~ msgid "Principality of Monaco"
#~ msgstr "Monacké kniežatstvo"

#~ msgid "Montserrat"
#~ msgstr "Montserrat"

#~ msgid "Kingdom of Morocco"
#~ msgstr "Marocké kráľovstvo"

#~ msgid "Mozambique"
#~ msgstr "Mozambik"

#~ msgid "Republic of Mozambique"
#~ msgstr "Mozambická republika"

#~ msgid "Myanmar"
#~ msgstr "Mjanmarsko"

#~ msgid "Union of Myanmar"
#~ msgstr "Mjanmarský zväz"

#~ msgid "Namibia"
#~ msgstr "Namíbia"

#~ msgid "Republic of Namibia"
#~ msgstr "Namíbijská republika"

#~ msgid "Nauru"
#~ msgstr "Nauru"

#~ msgid "Republic of Nauru"
#~ msgstr "Nauruská republika"

#~ msgid "Nepal"
#~ msgstr "Nepál"

#, fuzzy
#~ msgid "Federal Democratic Republic of Nepal"
#~ msgstr "Etiópska federatívna demokratická republika"

#~ msgid "Kingdom of the Netherlands"
#~ msgstr "Holandské kráľovstvo"

#~ msgid "Netherlands Antilles"
#~ msgstr "Holandské Antily"

#~ msgid "Republic of Nicaragua"
#~ msgstr "Nikaragujská republika"

#~ msgid "Niger"
#~ msgstr "Niger"

#~ msgid "Republic of the Niger"
#~ msgstr "Nigerská republika"

#~ msgid "Nigeria"
#~ msgstr "Nigéria"

#~ msgid "Federal Republic of Nigeria"
#~ msgstr "Nigérijská federatívna republika"

#~ msgid "Niue"
#~ msgstr "Niue"

#~ msgid "Republic of Niue"
#~ msgstr "Republika Niue"

#~ msgid "Norfolk Island"
#~ msgstr "Norfolk"

#~ msgid "Northern Mariana Islands"
#~ msgstr "Severné Mariány"

#~ msgid "Commonwealth of the Northern Mariana Islands"
#~ msgstr "Spoločenstvo Severných Marián"

#~ msgid "Kingdom of Norway"
#~ msgstr "Nórske kráľovstvo"

#~ msgid "Oman"
#~ msgstr "Omán"

#~ msgid "Sultanate of Oman"
#~ msgstr "Ománsky sultanát"

#~ msgid "Islamic Republic of Pakistan"
#~ msgstr "Pakistanská islamská republika"

#~ msgid "Palau"
#~ msgstr "Palau"

#~ msgid "Republic of Palau"
#~ msgstr "Palauská republika"

#~ msgid "Palestinian Territory, Occupied"
#~ msgstr "palestínske územie, Okupované"

#~ msgid "Occupied Palestinian Territory"
#~ msgstr "Okupované palestínske územie"

#~ msgid "Republic of Panama"
#~ msgstr "Panamská republika"

#~ msgid "Papua New Guinea"
#~ msgstr "Papua - Nová Guinea"

#~ msgid "Paraguay"
#~ msgstr "Paraguaj"

#~ msgid "Republic of Paraguay"
#~ msgstr "Paraguajská republika"

#~ msgid "Republic of Peru"
#~ msgstr "Peruánska republika"

#~ msgid "Republic of the Philippines"
#~ msgstr "Filipínska republika"

#~ msgid "Pitcairn"
#~ msgstr "Pitcairnove ostrovy"

#~ msgid "Republic of Poland"
#~ msgstr "Poľská republika"

#~ msgid "Portuguese Republic"
#~ msgstr "Portugalská republika"

#~ msgid "Puerto Rico"
#~ msgstr "Portoriko"

#~ msgid "Qatar"
#~ msgstr "Katar"

#~ msgid "State of Qatar"
#~ msgstr "Katarský štát"

#~ msgid "Russian Federation"
#~ msgstr "Ruská federácia"

#~ msgid "Rwanda"
#~ msgstr "Rwanda"

#~ msgid "Rwandese Republic"
#~ msgstr "Rwandská republika"

#~ msgid "Saint Helena"
#~ msgstr "Svätá Helena"

#~ msgid "Saint Kitts and Nevis"
#~ msgstr "Svätý Krištof a Nevis"

#~ msgid "Saint Lucia"
#~ msgstr "Svätá Lucia"

#~ msgid "Saint Pierre and Miquelon"
#~ msgstr "Saint Pierre a Miquelon"

#~ msgid "Saint Vincent and the Grenadines"
#~ msgstr "Svätý Vincent a Grenadíny"

#~ msgid "Samoa"
#~ msgstr "Samoa"

#~ msgid "Independent State of Samoa"
#~ msgstr "Samojský nezávislý štát"

#~ msgid "San Marino"
#~ msgstr "San Maríno"

#~ msgid "Republic of San Marino"
#~ msgstr "Sanmarínska republika"

#~ msgid "Sao Tome and Principe"
#~ msgstr "Svätý Tomáš a Princov ostrov"

#~ msgid "Democratic Republic of Sao Tome and Principe"
#~ msgstr "Demokratická republika Svätého Tomáša a Princovho ostrova"

#~ msgid "Kingdom of Saudi Arabia"
#~ msgstr "Saudskoarabské kráľovstvo"

#~ msgid "Senegal"
#~ msgstr "Senegal"

#~ msgid "Republic of Senegal"
#~ msgstr "Senegalská republika"

#~ msgid "Republic of Serbia"
#~ msgstr "Srbská republika"

#~ msgid "Seychelles"
#~ msgstr "Seychely"

#~ msgid "Republic of Seychelles"
#~ msgstr "Seychelská republika"

#~ msgid "Sierra Leone"
#~ msgstr "Sierra Leone"

#~ msgid "Republic of Sierra Leone"
#~ msgstr "Sierraleonská republika"

#~ msgid "Republic of Singapore"
#~ msgstr "Singapurská republika"

#~ msgid "Slovak Republic"
#~ msgstr "Slovenská republika"

#~ msgid "Republic of Slovenia"
#~ msgstr "Slovinská republika"

#~ msgid "Solomon Islands"
#~ msgstr "Šalamúnove ostrovy"

#~ msgid "Somalia"
#~ msgstr "Somálsko"

#~ msgid "Somali Republic"
#~ msgstr "Somálska republika"

#~ msgid "Republic of South Africa"
#~ msgstr "Juhoafrická republika"

#~ msgid "South Georgia and the South Sandwich Islands"
#~ msgstr "Južná Georgia a Južné Sandwichove ostrovy"

#~ msgid "Kingdom of Spain"
#~ msgstr "Španielske kráľovstvo"

#~ msgid "Democratic Socialist Republic of Sri Lanka"
#~ msgstr "Srílanská demokratická socialistická republika"

#~ msgid "Sudan"
#~ msgstr "Sudán"

#~ msgid "Republic of the Sudan"
#~ msgstr "Sudánska republika"

#~ msgid "Suriname"
#~ msgstr "Surinam"

#~ msgid "Republic of Suriname"
#~ msgstr "Surinamská republika"

#~ msgid "Svalbard and Jan Mayen"
#~ msgstr "Svalbard a Jan Mayen"

#~ msgid "Swaziland"
#~ msgstr "Svazijsko"

#~ msgid "Kingdom of Swaziland"
#~ msgstr "Svazijské kráľovstvo"

#~ msgid "Kingdom of Sweden"
#~ msgstr "Švédske kráľovstvo"

#~ msgid "Swiss Confederation"
#~ msgstr "Švajčiarska konfederácia"

#~ msgid "Syrian Arab Republic"
#~ msgstr "Sýrska arabská republika"

#~ msgid "Taiwan, Province of China"
#~ msgstr "Taiwan, provincia Číny"

#~ msgid "Republic of Tajikistan"
#~ msgstr "Tadžická republika"

#~ msgid "Tanzania, United Republic of"
#~ msgstr "Tanzánijská zjednotená republika"

#~ msgid "United Republic of Tanzania"
#~ msgstr "Tanzánijská zjednotená republika"

#~ msgid "Kingdom of Thailand"
#~ msgstr "Thajské kráľovstvo"

#~ msgid "Timor-Leste"
#~ msgstr "Východný Timor"

#~ msgid "Democratic Republic of Timor-Leste"
#~ msgstr "Východotimorská demokratická republika"

#~ msgid "Togo"
#~ msgstr "Togo"

#~ msgid "Togolese Republic"
#~ msgstr "Togská republika"

#~ msgid "Tokelau"
#~ msgstr "Tokelau"

#~ msgid "Tonga"
#~ msgstr "Tonga"

#~ msgid "Kingdom of Tonga"
#~ msgstr "Tongské kráľovstvo"

#~ msgid "Trinidad and Tobago"
#~ msgstr "Trinidad a Tobago"

#~ msgid "Republic of Trinidad and Tobago"
#~ msgstr "Republika Trinidadu a Tobaga"

#~ msgid "Republic of Tunisia"
#~ msgstr "Tuniská republika"

#~ msgid "Republic of Turkey"
#~ msgstr "Turecká republika"

#~ msgid "Turkmenistan"
#~ msgstr "Turkménsko"

#~ msgid "Turks and Caicos Islands"
#~ msgstr "Ostrovy Turks a Caicos"

#~ msgid "Tuvalu"
#~ msgstr "Tuvalu"

#~ msgid "Uganda"
#~ msgstr "Uganda"

#~ msgid "Republic of Uganda"
#~ msgstr "Ugandská republika"

#~ msgid "United Kingdom of Great Britain and Northern Ireland"
#~ msgstr "Spojené kráľovstvo Veľkej Británie a Severného Írska"

#~ msgid "United States of America"
#~ msgstr "Spojené štáty americké"

#~ msgid "United States Minor Outlying Islands"
#~ msgstr "Menšie odľahlé ostrovy Spojených štátov"

#~ msgid "Eastern Republic of Uruguay"
#~ msgstr "Uruguajská východná republika"

#~ msgid "Republic of Uzbekistan"
#~ msgstr "Uzbecká republika"

#~ msgid "Republic of Vanuatu"
#~ msgstr "Vanuatská republika"

#~ msgid "Bolivarian Republic of Venezuela"
#~ msgstr "Venezuelská bolívarovská republika"

#~ msgid "Socialist Republic of Viet Nam"
#~ msgstr "Vietnamská socialistická republika"

#~ msgid "Virgin Islands, British"
#~ msgstr "Panenské ostrovy, Britské"

#~ msgid "British Virgin Islands"
#~ msgstr "Britské Panenské ostrovy"

#~ msgid "Virgin Islands, U.S."
#~ msgstr "Panenské ostrovy, Americké"

#~ msgid "Virgin Islands of the United States"
#~ msgstr "Panenské ostrovy Spojených štátov"

#~ msgid "Wallis and Futuna"
#~ msgstr "Wallis a Futuna"

#~ msgid "Western Sahara"
#~ msgstr "Západná Sahara"

#~ msgid "Yemen"
#~ msgstr "Jemen"

#~ msgid "Republic of Yemen"
#~ msgstr "Jemenská republika"

#~ msgid "Zambia"
#~ msgstr "Zambia"

#~ msgid "Republic of Zambia"
#~ msgstr "Zambijská republika"

#~ msgid "Republic of Zimbabwe"
#~ msgstr "Zimbabwianska republika"

#, fuzzy
#~ msgid "British Antarctic Territory"
#~ msgstr "Britské indickooceánske územie"

#, fuzzy
#~ msgid "Burma, Socialist Republic of the Union of"
#~ msgstr "Vietnamská socialistická republika"

#, fuzzy
#~ msgid "French Southern and Antarctic Territories"
#~ msgstr "Francúzske južné a antarktické územia"

#, fuzzy
#~ msgid "German Democratic Republic"
#~ msgstr "Laoská ľudovodemokratická republika"

#, fuzzy
#~ msgid "Germany, Federal Republic of"
#~ msgstr "Nemecká spolková republika"

#, fuzzy
#~ msgid "Gilbert and Ellice Islands"
#~ msgstr "Ostrovy Turks a Caicos"

#, fuzzy
#~ msgid "Johnston Island"
#~ msgstr "Cookove ostrovy"

#, fuzzy
#~ msgid "Midway Islands"
#~ msgstr "Kajmanie ostrovy"

#, fuzzy
#~ msgid "Panama, Republic of"
#~ msgstr "Macedónska republika"

#, fuzzy
#~ msgid "Romania, Socialist Republic of"
#~ msgstr "Vietnamská socialistická republika"

#, fuzzy
#~ msgid "Serbia and Montenegro"
#~ msgstr "Čiernohorská republika"

#, fuzzy
#~ msgid "Upper Volta, Republic of"
#~ msgstr "Moldavská republika"

#, fuzzy
#~ msgid "Vatican City State (Holy See)"
#~ msgstr "Svätá stolica (Vatikánsky mestský štát)"

#, fuzzy
#~ msgid "Viet-Nam, Democratic Republic of"
#~ msgstr "Kórejská ľudovodemokratická republika"

#, fuzzy
#~ msgid "Wake Island"
#~ msgstr "Faerské ostrovy"

#, fuzzy
#~ msgid "Yemen, Democratic, People's Democratic Republic of"
#~ msgstr "Kórejská ľudovodemokratická republika"

#, fuzzy
#~ msgid "Yemen, Yemen Arab Republic"
#~ msgstr "Sýrska arabská republika"

#, fuzzy
#~ msgid "Yugoslavia, Socialist Federal Republic of"
#~ msgstr "Tanzánijská zjednotená republika"

#, fuzzy
#~ msgid "Zaire, Republic of"
#~ msgstr "Kórejská republika"

#~ msgid "The Transitional Islamic State of Afghanistan"
#~ msgstr "Dočasný afganský islamský štát"

#~ msgid "State of Bahrain"
#~ msgstr "Bahrajnské kráľovstvo"

#~ msgid "Kingdom of Nepal"
#~ msgstr "Nepálske kráľovstvo"

#~ msgid "Great Britain"
#~ msgstr "Spojené kráľovstvo"
