#use wml::debian::translation-check translation="4b462ea192fc355c557625a4edd8b02668ca91dd"
<define-tag description>actualización de seguridad</define-tag>
<define-tag moreinfo>
<p>Se encontró que los dos problemas de seguridad siguientes afectaban a
freeimage, una biblioteca gráfica:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-12211">CVE-2019-12211</a>

    <p>Desbordamiento de memoria dinámica («heap») provocado por una memcpy inválida en PluginTIFF. Atacantes
    desde ubicaciones remotas podrían aprovechar este defecto para desencadenar denegación de
    servicio u otras acciones con impacto no especificado a través de datos TIFF preparados.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-12213">CVE-2019-12213</a>

    <p>Agotamiento de la pila provocado por una recursividad no deseada en PluginTIFF. Atacantes
    desde ubicaciones remotas podrían aprovechar este defecto para desencadenar denegación de
    servicio a través de datos TIFF preparados.</p></li>

</ul>

<p>Para la distribución «antigua estable» (stretch), estos problemas se han corregido
en la versión 3.17.0+ds1-5+deb9u1.</p>

<p>Para la distribución «estable» (buster), estos problemas se han corregido en
la versión 3.18.0+ds2-1+deb10u1.</p>

<p>Le recomendamos que actualice los paquetes de freeimage.</p>

<p>Para información detallada sobre el estado de seguridad de freeimage, consulte
su página en el sistema de seguimiento de problemas de seguridad:
<a href="https://security-tracker.debian.org/tracker/freeimage">\
https://security-tracker.debian.org/tracker/freeimage</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4593.data"
