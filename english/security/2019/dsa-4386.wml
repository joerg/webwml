<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Multiple vulnerabilities were discovered in cURL, an URL transfer library.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-16890">CVE-2018-16890</a>

    <p>Wenxiang Qian of Tencent Blade Team discovered that the function
    handling incoming NTLM type-2 messages does not validate incoming
    data correctly and is subject to an integer overflow vulnerability,
    which could lead to an out-of-bounds buffer read.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-3822">CVE-2019-3822</a>

    <p>Wenxiang Qian of Tencent Blade Team discovered that the function
    creating an outgoing NTLM type-3 header is subject to an integer
    overflow vulnerability, which could lead to an out-of-bounds write.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-3823">CVE-2019-3823</a>

    <p>Brian Carpenter of Geeknik Labs discovered that the code handling
    the end-of-response for SMTP is subject to an out-of-bounds heap
    read.</p></li>

</ul>

<p>For the stable distribution (stretch), these problems have been fixed in
version 7.52.1-5+deb9u9.</p>

<p>We recommend that you upgrade your curl packages.</p>

<p>For the detailed security status of curl please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/curl">\
https://security-tracker.debian.org/tracker/curl</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4386.data"
# $Id: $
