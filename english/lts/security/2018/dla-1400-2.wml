<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>The security update of Tomcat 7 announced as DLA-1400-1 introduced a
regression for applications that make use of the Equinox OSGi
framework. The MANIFEST file of tomcat-jdbc.jar in libtomcat7-java
contains an invalid version number which was automatically derived
from the Debian package version. This caused an OSGi exception.</p>

<p>For Debian 8 <q>Jessie</q>, this issue has been fixed in version
7.0.56-3+really7.0.88-2.</p>

<p>We recommend that you upgrade your tomcat7 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1400-2.data"
# $Id: $
