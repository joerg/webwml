<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-12635">CVE-2017-12635</a>

      <p>Prevent non-admin users to give themselves admin privileges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-12636">CVE-2017-12636</a>

      <p>Blacklist some configuration options to prevent execution of
      arbitrary shell commands as the CouchDB user</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1.2.0-5+deb7u1.</p>

<p>We recommend that you upgrade your couchdb packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1252.data"
# $Id: $
