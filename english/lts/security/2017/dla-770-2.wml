<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Dawid Golunski discovered that PHPMailer, a popular library to send
email from PHP applications, allowed a remote attacker to execute
code if they were able to provide a crafted Sender address.</p>

<p>Note that for this issue also <a href="https://security-tracker.debian.org/tracker/CVE-2016-10045">CVE-2016-10045</a> was assigned, which is a
regression in the original patch proposed for <a href="https://security-tracker.debian.org/tracker/CVE-2016-10033">CVE-2016-10033</a>. Because
the origial patch was not applied in Debian, Debian was not vulnerable
to <a href="https://security-tracker.debian.org/tracker/CVE-2016-10045">CVE-2016-10045</a>.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
5.1-1.2.</p>

<p>We recommend that you upgrade your libphp-phpmailer packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-770-2.data"
# $Id: $
