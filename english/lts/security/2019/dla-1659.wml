<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A remote code execution vulnerability exists in PHP's built-in phar
stream wrapper when performing file operations on an untrusted phar://
URI. Some Drupal code (core, contrib, and custom) may be performing
file operations on insufficiently validated user input, thereby being
exposed to this vulnerability.</p>

<p>With this update a new replacement stream wrapper from typo3 project
is used instead of the built-in one.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
7.32-1+deb8u14.</p>

<p>We recommend that you upgrade your drupal7 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1659.data"
# $Id: $
