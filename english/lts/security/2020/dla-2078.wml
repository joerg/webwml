<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>An untrusted deserialization was found in the
org.apache.xmlrpc.parser.XmlRpcResponseParser:addResult method of Apache
XML-RPC (aka ws-xmlrpc) library. A malicious XML-RPC server could target
a XML-RPC client causing it to execute arbitrary code.</p>

<p>Clients that expect to get server-side exceptions need to set the
enabledForExceptions property to true in order to process serialized
exception messages again.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
3.1.3-7+deb8u1.</p>

<p>We recommend that you upgrade your libxmlrpc3-java packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2078.data"
# $Id: $
